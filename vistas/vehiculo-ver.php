<?php
  include_once("panel/modelo/Vehiculo.php");
  include_once("panel/modelo/Modelo.php");
  $veh = new Vehiculo();
  
  $id = $_GET['id'];
  $VEH = $veh->findById($id);
  if($VEH == false){
	echo "<script>window.location ='index.php';</script>";
	exit(1);
  }

  $mod = new Modelo();
  $modelo = $mod->findById($VEH['id_modelo']);
  $modelo = $modelo['modelo'];
?>

<section class="ftco-section ftco-project bg-light" id="vehiculos">
	<div class="container px-md-5">
		<div class="row justify-content-center pb-5">
			<div class="col-md-12 heading-section text-center ftco-animate">
				<span class="subheading">Vehículos</span>
				<h3><?php echo strtoupper($VEH['nom_veh']);?></h3>
			</div>
		</div>

		<div class="col-xs-12 col-md-9 offset-md-1 p-0 page pb-4">
			<div class="sp-loading"><img src="static/img/sp-loading.gif" alt=""></div>
			<div class="sp-wrap">
				<a href="<?php echo $VEH['img1'];?>">
					<img src="<?php echo $VEH['img1'];?>" alt="" width="100%">
				</a>
				<?php if($VEH['img2'] != null) { ?>
					<a href="<?php echo $VEH['img2'];?>"><img src="<?php echo $VEH['img2'];?>" alt="" width="100%"></a>
				<?php } ?>
				<?php if($VEH['img3'] != null) { ?>
					<a href="<?php echo $VEH['img3'];?>"><img src="<?php echo $VEH['img3'];?>" alt="" width="100%"></a>
				<?php } ?>
				<?php if($VEH['img4'] != null) { ?>
					<a href="<?php echo $VEH['img4'];?>"><img src="<?php echo $VEH['img4'];?>" alt="" width="100%"></a>
				<?php } ?>
				<?php if($VEH['img5'] != null) { ?>
					<a href="<?php echo $VEH['img5'];?>"><img src="<?php echo $VEH['img5'];?>" alt="" width="100%"></a>
				<?php } ?>
			</div>
		</div>
	</div>

	<div class="container">
		<div class="row mb-3">
			<div class="col-sm-12 text-center">
				<h3 class="title-d">Precio: <small><?php if($VEH['pre_veh'] == null || $VEH['pre_veh'] == 0) echo 'A consultar';else echo $VEH['pre_veh']." USD";?></small>
				<a href="#md-interesa" class="btn btn-primary" data-toggle="modal" data-target="#md-interesa">Me interesa</a></h3>
			</div>
		</div>

		<div class="row tabulation mt-4 ftco-animate">
			<div class="col-md-4">
				<ul class="nav nav-pills nav-fill d-md-flex d-block flex-column">
					<li class="nav-item text-left">
						<a class="nav-link active py-4" data-toggle="tab" href="#services-1"><span class="flaticon-specification"></span> Especificaciones</a>
					</li>

					<li class="nav-item text-left">
						<a class="nav-link py-4" data-toggle="tab" href="#services-2"><span class="flaticon-description mr-2"></span> Descripción</a>
					</li>

					<li class="nav-item text-left">
						<a class="nav-link py-4" data-toggle="tab" href="#services-3"><span class="flaticon-business mr-2"></span> Flexibilidad</a>
					</li>

					<li class="nav-item text-left">
						<a class="nav-link py-4" data-toggle="tab" href="#services-4"><span class="flaticon-steering-wheel mr-2"></span> Diseño</a>
					</li>

					<li class="nav-item text-left">
						<a class="nav-link py-4" data-toggle="tab" href="#services-5"><span class="flaticon-rating mr-2"></span> Seguridad</a>
					</li>
				</ul>
			</div>

			<div class="col-md-8">
				<div class="tab-content">
				<div class="tab-pane container p-0 active" id="services-1">
					<h3>Especificaciones</h3>
					<h5><b>Modelo:</b> <span><?php echo strtoupper($modelo);?></span></h5>

					<h5><b>Transmisión:</b> <span><?php echo strtoupper($VEH['tra_veh']);?></span></h5>

				<?php if($VEH['sto_veh'] == 1) { ?> 
					<h5><b>¿ Disponible en exiauto ?</b> <span><?php echo ($VEH['sto_veh']==1)?'Si':'No';?></span></h5>
				<?php } ?>
					<h5><b>Año:</b> <span><?php echo ($VEH['ano_veh']);?></span></h5>
					<h5><b>Tipo:</b> <span><?php echo ($VEH['imp_veh']==1)?'IMPORTADO':'NACIONAL';?></span></h5>
				 <?php if($VEH['doc'] != null) { ?> 
					<h5><b>Documento detallado </b> <span><a href='<?php echo ($VEH['doc']);?>' style="color:red;" target="__blank">Click aquí</a></span></h5>
				<?php } ?>
				</div>

				<div class="tab-pane container p-0 fade" id="services-2">
					<h3>Descripción</h3>
					<p class="text-justify"><?php echo nl2br($VEH['des_veh']);?></p>
				</div>

				<div class="tab-pane container p-0 fade" id="services-3">
					<h3>Flexibilidad</h3>
					<p class="text-justify"><?php echo nl2br($VEH['fle_veh']);?></p>
				</div>

				<div class="tab-pane container p-0 fade" id="services-4">
					<h3>Diseño</h3>
					<p class="text-justify"><?php echo nl2br($VEH['dis_veh']);?></p>
				</div>

				<div class="tab-pane container p-0 fade" id="services-5">
					<h3>Seguridad</h3>
					<p class="text-justify"><?php echo nl2br($VEH['seg_veh']);?></p>
				</div>
				</div>
			</div>
		</div>
	</div>
</section>


<div id="md-interesa" class="modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5>Me interesa</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="a consultarmodal-body" style="text-align:center;">
				<h5>Para mas información comunícate con nosotros al:<b> 0243-2363061</b></h5><br>
				
				<h5>Escríbenos <a href="mailto:ventas@toyomayamaracay.com?Subject=Me interesa este vehículo"><b>ventas@toyomayamaracay.com</b></a></h5>
			</div>
		</div>
	</div>
</div>