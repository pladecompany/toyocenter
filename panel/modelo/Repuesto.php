<?php
  include_once("Orm.php");
  include_once("Conexion.php");

  class Repuesto{
    private $tabla = "repuestos";
    public $data = [];
    public $orm = null;

    public function Repuesto($tabla){
      $this->orm = new Orm(new Conexion());
      $tihs->data = [];
    }

    public function removeById($id){
      return $this->orm->eliminar('id', $id, $this->tabla);
    }

    public function removeModelos($id){
      return $this->orm->eliminar('id_repuesto', $id,"asignar_modelos_repuestos");
    }

    public function findSolicitud($id){
      $sql = "SELECT S.*, R.id id_repuesto, R.nom_rep FROM solicitud_repuestos S, repuestos R WHERE S.id='$id' AND S.id_repuesto=R.id;";
      $r = $this->orm->consultaPersonalizada($sql);
      if($r->num_rows==1){
        return $r->fetch_assoc();
      }else{
        return false;
      }
    }
    public function findById($id){
      $sql = "SELECT * FROM " . $this->tabla . " WHERE id='$id';";
      $r = $this->orm->consultaPersonalizada($sql);
      if($r->num_rows==1){
        return $r->fetch_assoc();
      }else{
        return false;
      }
    }

    public function asignarModelos($idr, $modelos, $anos){
      $this->removeModelos($idr);
      for($i=0;$i<count($modelos);$i++){

        $anos_coma = explode(",", $anos[$i]); 

        $anos_txt = "";
        for($j=0;$j<count($anos_coma);$j++){
          $anos_guio = explode("-", $anos_coma[$j]);

          if(count($anos_guio)==2){
            $ini = $anos_guio[0];
            $fin = $anos_guio[1];
            for($k=$ini;$k<=$fin;$k++)
              $anos_txt.= trim($k).",";
          }else{
            $anos_txt.= trim($anos_coma[$j]).",";
          }
        }
        $sql = "INSERT INTO asignar_modelos_repuestos values(null, $idr, ".$modelos[$i].",'".$anos[$i]."', '$anos_txt');";
        $this->orm->insertarPersonalizado($sql);
      }
      return $i;
    }

    public function getModelosAsignados($idr){
      $sql = "SELECT *, A.id as idm FROM asignar_modelos_repuestos A, modelos_repuestos M WHERE A.id_modelo=M.id AND A.id_repuesto=$idr;";
      return $this->orm->consultaPersonalizada($sql);
    }

    public function getAnosModelo($idm){
      $sql = "SELECT * FROM asignar_modelos_repuestos WHERE id_modelo='$idm';";
      $r =  $this->orm->consultaPersonalizada($sql);
      $a=[];
      while($f = $r->fetch_assoc()){
        $anos = explode(",", $f['anos_txt']);
        for($i=0;$i<count($anos);$i++){
          if($this->existeAno($a, trim($anos[$i]))==false)
            $a[] = $anos[$i];
        }
      }
      return $a;
    }

    public function getRepuestosByAno($idm, $ano){
      $sql = "SELECT R.* FROM asignar_modelos_repuestos A, repuestos R WHERE A.id_repuesto=R.id AND A.id_modelo=$idm AND A.anos_txt like '%$ano%' AND R.est_rep=1";
      return $this->orm->consultaPersonalizada($sql);
    }

    public function existeAno($arreglo, $ano){
      $ano = intval($ano);
      for($i=1;$i<count($arreglo);$i++){
        if(intval($arreglo[$i]) == $ano)
          return true;
      }
      return false;
    }

    public function getModelosRepuestos(){
      $sql = "SELECT * FROM modelos_repuestos ORDER BY modelo;";
      return $this->orm->consultaPersonalizada($sql);
    }

    public function cambiarEstatus($est, $id){
      $sql = "UPDATE solicitud_repuestos SET estatus=1 WHERE id='$id';";
      return $this->orm->editarPersonalizado($sql);
    }

    public function insertarSolicitud(){
      $sql = "INSERT INTO solicitud_repuestos VALUES(";
      $i = 0;
      $n = count($this->data);
      foreach($this->data as $key => $index){
        $i++;
        if($index == "")
          $sql.= "null";
        else
          $sql.= "'$index'";
        if($i < $n){
          $sql.= ",";
        }
      }
      $sql.= ");";
      return $this->orm->insertarPersonalizado($sql);
    }

    public function solicitudesGenerales($f1, $f2){
      $sql = "SELECT * FROM solicitud_repuestos WHERE fec_reg_sol>='$f1 00:00:00' AND fec_reg_sol<='$f2 23:59:59' ORDER BY fec_reg_sol DESC;";
      return $this->orm->consultaPersonalizada($sql);
    }


    public function fetchAll(){
      $sql = "SELECT *, R.id as idr FROM ".$this->tabla." R, categorias_repuestos C WHERE R.id_categoria=C.id ORDER BY R.id;";
      return $this->orm->consultaPersonalizada($sql);
    }

    public function getRepuestosBYModelo($idm){
      $sql = "SELECT * FROM asignar_modelos_repuestos A, repuestos R WHERE A.id_repuesto=R.id AND A.id_modelo='$idm' AND R.est_rep=1;";
      return $this->orm->consultaPersonalizada($sql);
    }

    public function getRepuestosBYNombre($nom){
      $sql = "SELECT * FROM repuestos WHERE (nom_rep LIKE '%$nom%' OR cod_rep like '%$nom%') AND est_rep=1;";
      return $this->orm->consultaPersonalizada($sql);
    }

    public function getRepuestosBYNombreModelo($nom, $idm){
      $sql = "SELECT R.* FROM repuestos R, asignar_modelos_repuestos A WHERE (nom_rep LIKE '%$nom%' OR cod_rep like '%$nom%') AND R.est_rep=1 AND R.id=A.id_repuesto AND A.id_modelo=$idm;";
      return $this->orm->consultaPersonalizada($sql);
    }

    public function getUltimosPublicados($limit){
      $sql = "SELECT * FROM repuestos WHERE est_rep=1 ORDER BY id DESC LIMIT $limit;";
      return $this->orm->consultaPersonalizada($sql);
    }

    public function getRepuestosBYCategoria($idc){
      $sql = "SELECT * FROM repuestos WHERE id_categoria='$idc' AND est_rep=1;";
      return $this->orm->consultaPersonalizada($sql);
    }

    public function fetchAllActivas(){
      $sql = "SELECT * FROM ".$this->tabla." WHERE est_noti='1' ORDER BY id DESC;";
      return $this->orm->consultaPersonalizada($sql);
    }

    public function save(){
      $sql = "INSERT INTO " . $this->tabla ." VALUES(";
      $i = 0;
      $n = count($this->data);
      foreach($this->data as $key => $index){
        $i++;
        if($index == "")
          $sql.= "null";
        else
          $sql.= "'$index'";
        if($i < $n){
          $sql.= ",";
        }
      }
      $sql.= ");";
      return $this->orm->insertarPersonalizado($sql);
    }

      public function edit($id){
        $sql = "UPDATE ".$this->tabla." SET ";
        $i = 0;
        $n = count($this->data);
        foreach($this->data as $key => $index){
          $i++;
          $sql.= $key."='$index'";
          if($i < $n){
            $sql.= ",";
          }
        }
      $sql.= " WHERE id='$id';";
      return $this->orm->editarPersonalizado($sql);

    }

  }
?>
