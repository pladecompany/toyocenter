<?php
  include_once("panel/modelo/Noticia.php");
  $noti_leer = new Noticia();
  
  $idn = $_GET['id'];
  $NOTI = $noti_leer->findById($idn);
  if($NOTI == false){
	echo "<script>window.location ='index.php';</script>";
	exit(1);
  }
?>

<section class="ftco-section mt-3">
	<div class="container">
		<div class="row">
			<div class="col-lg-8 ftco-animate">
				<p class="mb-0">
					<img src="<?php echo $NOTI['img'];?>" alt="<?php echo $NOTI['titulo'];?>" class="img-fluid">
				</p>
				<h4 class="titulo-noticia mb-2"><?php echo $NOTI['titulo'];?></h4>
				<p class="text-justify"><?php echo nl2br($NOTI['descripcion']); ?></p>
				<small>Publicado el <?php echo $NOTI['fec_reg_noti'];?></small>
			</div>

			<div class="col-lg-4 ftco-animate">
				<div class="sidebar-box ftco-animate">
					<h3>Más noticias</h3>
					<div class="block-21 mb-4 d-flex">
						<a class="blog-img mr-4"><img src="<?php echo $NOTI['img'];?>" alt="<?php echo $NOTI['titulo'];?>" class="img-fluid"></a>
						<div class="text">
							<h6 class="heading"><a href="#"><?php echo $NOTI['titulo'];?></a></h6>
							<div class="meta">
							<div><a href="#"><span class="icon-calendar"></span> Mar. 15, 2020</a></div>
							</div>
						</div>
					</div><hr>

					<div class="block-21 mb-4 d-flex">
						<a class="blog-img mr-4"><img src="<?php echo $NOTI['img'];?>" alt="<?php echo $NOTI['titulo'];?>" class="img-fluid"></a>
						<div class="text">
							<h6 class="heading"><a href="#"><?php echo $NOTI['titulo'];?></a></h6>
							<div class="meta">
								<div><a href="#"><span class="icon-calendar"></span> Mar. 15, 2020</a></div>
							</div>
						</div>
					</div><hr>

					<div class="block-21 mb-4 d-flex">
						<a class="blog-img mr-4"><img src="<?php echo $NOTI['img'];?>" alt="<?php echo $NOTI['titulo'];?>" class="img-fluid"></a>
						<div class="text">
							<h6 class="heading"><a href="#"><?php echo $NOTI['titulo'];?></a></h6>
							<div class="meta">
							<div><a href="#"><span class="icon-calendar"></span> Mar. 15, 2020</a></div>
							</div>
						</div>
					</div><hr>
				</div>
			</div>
		</div>
	</div>
</section>