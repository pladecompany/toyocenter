<?php 
  include_once("panel/modelo/Noticia.php");
  $notit = new Noticia();
  $rnn = $notit->fetchAllActivas();
?>

<section class="ftco-section" id="noticias">
	<div class="container">
		<div class="row justify-content-center mb-5 pb-2">
			<div class="col-md-8 text-center heading-section ftco-animate">
				<span class="subheading">Noticias</span>
				<h2 class="mb-4">Lo más reciente</h2>
			</div>
		</div>

		<div class="row">
			<?php
				while($ffn = $rnn->fetch_assoc()){
                  $f = $ffn['fec_reg_noti'];
                $mes = date("M", strtotime($ffn['fec_reg_noti']));
                $fec = explode("-", $ffn['fec_reg_noti']); 
			?>
				<div class="col-md-6 col-lg-4 ftco-animate">
					<div class="blog-entry">
						<img src="<?php echo $ffn['img'];?>" alt="" class="block-20 d-flex align-items-end">
					
						<div class="meta-date text-center p-2">
							<span class="day"><?php echo $fec[2];?></span>
							<span class="mos"><?php echo $mes;?></span>
							<span class="yr"><?php echo $fec[0];?></span>
						</div>
					</div>

					<div class="text border border-top-0 p-4">
						<p class="heading">
							<?php echo $ffn['titulo'];?>
						</p>
						<div class="d-flex align-items-center mt-4">
							<p class="mb-0"><a href="?op=noticia-ver&id=<?php echo $ffn['id'];?>" class="btn btn-primary">Leer más <span class="ion-ios-arrow-round-forward"></span></a></p>
						</div>
					</div>
				</div>
			<?php
			  }
			?>
        </div>
    </div>
</section>
