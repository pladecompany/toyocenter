<section class="ftco-section ftco-no-pt ftco-no-pb ftco-consult" id="servicio">
	<div class="container">
		<div class="row d-flex no-gutters align-items-stretch consult-wrap">
			<div class="col-md-5 wrap-about align-items-stretch d-flex">
				<div class="ftco-animate bg-primary align-self-stretch px-4 py-5 w-100" style="background: #ff0119 !important;">
					<h2 class="heading-white mb-4 text-center">Servicio <b>TOYOTA</b></h2>
					<div class="text-center">
						<img src="static/img/logo.jpg" width="50%">
					</div>
				</div>
			</div>

			<div class="col-md-7 wrap-about ftco-animate align-items-stretch d-flex">
				<div class="bg-white p-5">
					<div class="row">
						<div class="col-lg-6">
							<div class="services">
								<div class="icon mt-2 d-flex align-items-center"><span class="flaticon-technician"></span></div>
								<div class="text media-body">
									<h3>Personal técnico especializado</h3>
								</div>
							</div>

							<div class="services">
								<div class="icon mt-2"><span class="flaticon-team"></span></div>
								<div class="text media-body">
									<h3>Asesoramiento profesional</h3>
								</div>
							</div>
						</div>

						<div class="col-lg-6">
							<div class="services">
								<div class="icon mt-2"><span class="flaticon-team-1"></span></div>
								<div class="text media-body">
									<h3>Equipos y herramientas de alta tecnología</h3>
								</div>
							</div>

							<div class="services">
								<div class="icon mt-2"><span class="flaticon-agreement"></span></div>
								<div class="text media-body">
									<h3>Rapidez y calidad</h3>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>