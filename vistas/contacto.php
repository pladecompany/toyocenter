<section class="hero-wrap hero-wrap-2" style="background-image: url('static/img/v2.jpg');">
	<div class="overlay"></div>
	<div class="container">
		<div class="row no-gutters slider-text align-items-center justify-content-center">
			<div class="col-md-9 ftco-animate text-center">
				<h1 class="mb-2 bread">Contacto</h1>
				<p class="breadcrumbs"><span class="mr-2"><a href="index.php">INICIO <i class="ion-ios-arrow-forward"></i></a></span> <span>Contacto <i class="ion-ios-arrow-forward"></i></span></p>
			</div>
		</div>
	</div>
</section>

<section class="ftco-section contact-section">
	<div class="container">
		<div class="row d-flex mb-5 contact-info justify-content-center">
			<div class="col-md-10">
				<div class="row mb-5">
					<div class="col-md-4 text-center d-flex">
						<div class="border w-100 p-4">
							<div class="icon">
								<span class="icon-map-o"></span>
							</div>
							<p><span>Dirección:</span> Av. Bolivar Este #82 sector centro frente al liceo Militar; diagonal al C.C Parque Aragua; Entrada por la Av. Miranda, a 50 metros de Av. Fuerza Aérea.</p>
						</div>
					</div>

					<div class="col-md-4 text-center d-flex">
						<div class="border w-100 p-4">
							<div class="icon">
								<span class="icon-tablet"></span>
							</div>
							<p><span>Teléfono:</span> <a href="#">Llámanos: 0243-2331465 <br> 0243-2331467 <br> 0243-2331468</a></p>
						</div>
					</div>

					<div class="col-md-4 text-center d-flex">
						<div class="border w-100 p-4">
							<div class="icon">
								<span class="icon-envelope-o"></span>
							</div>
							<p><span>Email:</span> <a href="mailto:contacto@toyocenter.com">contacto@toyocenter.com</a></p>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="col-md-12">
			<h2 class="text-center">Sí deseas comunicarte con nosotros <br><b>envíanos un mensaje</b></h2>
		</div>

		<div class="row">
			<div class="col-md-7 col-sm-12">
				<form class="form-a contactForm border p-5 contact-form" action="" method="post" role="form">
					<div id="errormessage"></div>
					<div class="row">
						<div class="col-md-6 mb-3">
							<div class="form-group">
								<input type="text" name="name" class="form-control form-control-lg form-control-a" placeholder="Nombre" data-rule="minlen:4" data-msg="Escribe mínimo 3 carácteres">
								<div class="validation"></div>
							</div>
						</div>

						<div class="col-md-6 mb-3">
							<div class="form-group">
								<input name="email" type="email" class="form-control form-control-lg form-control-a" placeholder="Correo electrónico" data-rule="email" data-msg="Escribe un email válido">
								<div class="validation"></div>
							</div>
						</div>

						<div class="col-md-12 mb-3">
							<div class="form-group">
								<input type="url" name="subject" class="form-control form-control-lg form-control-a" placeholder="Asunto" data-rule="minlen:4" data-msg="Escribe mínimo 5 carácteres">
								<div class="validation"></div>
							</div>
						</div>
						
						<div class="col-md-12 mb-3">
							<div class="form-group">
								<textarea name="message" class="form-control" name="message" cols="45" rows="8" data-rule="required" data-msg="Escribe mínimo 3 carácteres" placeholder="Mensaje"></textarea>
								<div class="validation"></div>
							</div>
						</div>
						<div class="col-md-12 mb-4">
							<button type="submit" class="btn btn-primary">Enviar mensaje</button>
						</div>
					</div>
				</form>
			</div>

			<div class="col-md-5 col-sm-12">
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3926.16764759843!2d-67.59076398486171!3d10.248059392681402!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8e803ca3ff6f7fed%3A0x331be4118a56825d!2sToyoCenter!5e0!3m2!1ses!2sve!4v1587654623303!5m2!1ses!2sve" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
			</div>
		</div>
	</div>
</section>