$(document).ready(function(){

          $("#tipo_cita").change(function(){
              $(".kilometros").hide();
              var val = $("#tipo_cita option:selected").attr('val');
              $("#contenedor_kilometraje").show();
              $("#contenedor_revision").show();
              $(".kilo").remove();

            if(val == "mat_ped"){
              $("#contenedor_revision").hide();
              $(".kilometros").each(function(index){
                var tip = $(this).attr('tip');
                if(tip == 'per')
                  $(this).show();
              });
            }else if(val == "rev_gen"){
              $("#contenedor_kilometraje").hide();
            }else if(val == "gar_tdv"){
              $("#contenedor_revision").hide();
              $(".kilometros").each(function(index){
                var tip = $(this).attr('tip');
                if(tip == 'gar')
                  $(this).show();
              });
            }else if(val == "gar_ser"){
              $("#contenedor_kilometraje").hide();
            }
          });

});
