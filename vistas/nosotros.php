<section class="ftco-section ftco-about ftco-no-pt ftco-no-pb ftco-counter" id="nosotros">
	<div class="container consult-wrap">
		<div class="row d-flex align-items-stretch">
			<div class="col-md-6 wrap-about align-items-stretch d-flex">
				<div class="img" style="background-image: url(static/img/1.jpg);"></div>
			</div>
			<div class="col-md-6 wrap-about ftco-animate py-md-5 pl-md-5">
				<div class="heading-section mb-4">
					<span class="subheading">Nosotros</span>
					<h2>TOYOCENTER</h2>
				</div>
				<p></p>

				<div class="tabulation-2 mt-4">
					<ul class="nav nav-pills nav-fill d-md-flex d-block">
						<li class="nav-item">
							<a class="nav-link active py-2" data-toggle="tab" href="#home1"><span class="ion-ios-home mr-2"></span> Misión</a>
						</li>
						<li class="nav-item px-lg-2">
							<a class="nav-link py-2" data-toggle="tab" href="#home2"><span class="ion-ios-person mr-2"></span> Visión</a>
						</li>
					</ul>

					<div class="tab-content bg-light rounded mt-2">
						<div class="tab-pane container p-0 active" id="home1">
							<p><b>Toyocenter C.A</b> mantiene como premisa esencial prestar servicio de primera, pues su personal calificado y especializado en vehículos de la marca Toyota junto a los equipos y herramientas de alta tecnología conforma una estructura integral que satisface las necesidades de los clientes.</p>
							<p>De igual forma, procura ofrecer a nuestros clientes rapidez y calidad en nuestros servicios, optimizando continuamente los procesos, capacitando y proyectando a nuestro personal.</p>	
						</div> 

						<div class="tab-pane container p-0 fade" id="home2">
							<p>Ser el concesionario automotor de mayor éxito en ventas de vehículos nuevos, repuestos y dando servicio a nivel regional, buscando siempre la satisfacción total a nuestros clientes, a través de la gente inspirada, manteniendo su rentabilidad por encima de los estándares.</p>
							<p>De igual forma, busca alcanzar el liderazgo como empresa de servicio a corto plazo a través de la utilización de la mano de obra calificada, el esmero por mantener informado y capacitado al personal de los avances tecnológicos y la preocupación por adquirir los equipos y herramientas necesarias y adecuadas para desempeñar un trabajo acorde a las exigencias del cliente.</p>
						</div>
					</div>
				</div>

				<div class="row mt-5 text-center" id="section-counter">
					<div class="col-md d-flex justify-content-center counter-wrap ftco-animate">
						<div class="block-18">
							<div class="icon"><span class="flaticon-doctor"></span></div>
						<div class="text">
							<strong class="number" data-number="18">0</strong>
							<span>Años de experiencia</span>
						</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
