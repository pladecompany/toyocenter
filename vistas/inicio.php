	<section class="home-slider owl-carousel">
		<div class="slider-item" style="background-image:url(static/img/slider-1.jpg);">
			<div class="overlay"></div>
				<div class="container">
					<div class="row no-gutters slider-text align-items-center justify-content-start" data-scrollax-parent="true">
					<div class="col-md-7 ftco-animate mb-md-5">
						<span class="subheading">Toyocenter</span>
						<h1 class="mb-4">Bienvenido a nuestro concesionario TOYOTA</h1>
					</div>
				</div>
			</div>
		</div>

		<div class="slider-item" style="background-image:url(static/img/slider-2.jpg);">
			<div class="overlay"></div>
				<div class="container">
					<div class="row no-gutters slider-text align-items-center justify-content-start" data-scrollax-parent="true">
					<div class="col-md-7 ftco-animate mb-md-5">
						<span class="subheading">Repuestos</span>
						<h1 class="mb-4">Variedad en repuestos originales TOYOTA</h1>
						<p><a href="?op=repuestos" class="btn btn-primary px-4 py-3 mt-3">Solicitar / preguntar</a></p>
					</div>
				</div>
			</div>
		</div>

		<div class="slider-item" style="background-image:url(static/img/slider-3.jpg);">
			<div class="overlay"></div>
				<div class="container">
					<div class="row no-gutters slider-text align-items-center justify-content-start" data-scrollax-parent="true">
					<div class="col-md-7 ftco-animate mb-md-5">
						<span class="subheading">Vehículos</span>
						<h1 class="mb-4">Solicita el automóvil que necesitas</h1>
						<p><a href="?op=vehiculos" class="btn btn-primary px-4 py-3 mt-3">Solicitar / preguntar</a></p>
					</div>
				</div>
			</div>
		</div>
	</section>

	<?php include("servicio.php"); ?>
		
	<section class="ftco-intro ftco-no-pb img">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-md-10 text-center heading-section heading-section-white ftco-animate">
					<h2 class="mb-0">TOYOTA tú mejor aliado</h2>
				</div>
			</div>	
		</div>
	</section>

	<?php
		include("nosotros.php");
	?>


	<section class="ftco-section testimony-section" id="vehiculos">
		<div class="container-fluid px-md-5">
			<div class="row justify-content-center">
				<div class="col-md-8 text-center heading-section ftco-animate">
					<span class="subheading">Vehículos</span>
					<h2 class="mb-4">Variedad de vehículos</h2>
				</div>
			</div>


			<div class="row ftco-animate justify-content-center">
				<div class="col-md-12">
					<div class="carousel-testimony owl-carousel">
						<?php
							$veh = new Vehiculo();
							$rv = $veh->fetchVehiculosLanding(6);
							while($fv = $rv->fetch_assoc()){
						?>
						<div class="item">
							<div class="testimony-wrap">
								<img src="<?php echo $fv['img1'];?>" class="img-vehiculo" alt="" class="user-img">
								<div class="testimony-text pl-4">
									<a href="?op=vehiculo-ver&id=<?php echo $fv['id'];?>" class="clr_white"><h6><?php echo $fv['nom_veh'];?></h6></a>
									<span><?php echo $fv['ano_ve'];?></span>
								</div>
							</div>
						</div>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</section>


	<section class="ftco-intro ftco-no-pb img">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-lg-9 col-md-8 d-flex align-items-center heading-section heading-section-white ftco-animate">
					<h2 class="mb-3 mb-md-0">Solicita o pregunta por un repuesto</h2>
				</div>

				<div class="col-lg-3 col-md-4 ftco-animate">
					<p class="mb-0"><a href="?op=contacto" class="btn btn-secondary py-3 px-4">Escríbenos</a></p>
				</div>
			</div>	
		</div>
	</section>

	<?php 
	  include_once("panel/modelo/Noticia.php");
	  $notit = new Noticia();
	  $rnn = $notit->fetchAllActivas();
	?>


	<section class="ftco-section" id="noticias">
		<div class="container">
			<div class="row justify-content-center mb-5 pb-2">
				<div class="col-md-8 text-center heading-section ftco-animate">
					<span class="subheading">Noticias</span>
					<h2 class="mb-4">Lo más reciente</h2>
				</div>
			</div>

			<div class="row">
				<?php
					while($ffn = $rnn->fetch_assoc()){
	                  $f = $ffn['fec_reg_noti'];
	                $mes = date("M", strtotime($ffn['fec_reg_noti']));
	                $fec = explode("-", $ffn['fec_reg_noti']); 
				?>
					<div class="col-md-6 col-lg-4 ftco-animate">
						<div class="blog-entry">
							<img src="<?php echo $ffn['img'];?>" alt="" class="block-20 d-flex align-items-end">
						
							<div class="meta-date text-center p-2">
								<span class="day"><?php echo $fec[2];?></span>
								<span class="mos"><?php echo $mes;?></span>
								<span class="yr"><?php echo $fec[0];?></span>
							</div>
						</div>

						<div class="text border border-top-0 p-4">
							<p class="heading">
								<?php echo $ffn['titulo'];?>
							</p>
							<div class="d-flex align-items-center mt-4">
								<p class="mb-0"><a href="?op=noticia-ver&id=<?php echo $ffn['id'];?>" class="btn btn-primary">Leer más <span class="ion-ios-arrow-round-forward"></span></a></p>
							</div>
						</div>
					</div>
				<?php
				  }
				?>

				<div class="col s12 text-center mt-3">
					<a href="?op=noticias" class="btn btn-primary">Ver todas</a>
				</div>
			</div>
		</div>
	</section>


	<?php
		include_once("promo.php");
	?>
