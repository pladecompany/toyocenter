<?php
	error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING ^ E_DEPRECATED);
	
	$opcion = $_GET['op'];

    if(($opcion =="ver_reclamo" ||$opcion == "cita" || $opcion=="perfil" || $opcion=="misvehiculos" || $opcion == "inicio_log" ||$opcion == "reclamos") && !isset($_SESSION['log'])){
      echo "<script>window.location ='index.php';</script>";
      exit(1);
    }

	switch ($opcion) {
      case "ver_reclamo":
        $ruta="vistas/ver_reclamo.php";
        break;
      case "inicio_log":
        $ruta="vistas/inicio_log.php";
        break;
      case "misvehiculos":
        $ruta="vistas/misvehiculos.php";
        break;
      case "cita":
        $ruta="vistas/miscitas.php";
        break;
      case "perfil":
        $ruta="vistas/perfil.php";
        break;
      case "reclamos":
        $ruta="vistas/reclamos.php";
        break;
      case "inicio":
        $ruta="vistas/inicio.php";
        break;
      case "contacto":
        $ruta="vistas/contacto.php";
        break;

      case "mantenimiento":
        $ruta="vistas/mantenimiento.php";
        break;

      case "latoneria":
        $ruta="vistas/latoneria.php";
        break;

      case "repuestos":
        $ruta="vistas/repuestos.php";
        break;

      case "noticia-ver":
        $ruta="vistas/noticia-ver.php";
        break;

      case "vehiculo-ver":
        $ruta="vistas/vehiculo-ver.php";
        break;

      case "vehiculos":
        $ruta="vistas/vehiculos.php";
        break;

      case "noticias":
        $ruta="vistas/noticias.php";
        break;

      case "solicitarcita":
        $ruta="vistas/solicitarcita.php";
        break;

      case "cambioexpress":
        $ruta="vistas/cambioexpress.php";
        break;

      case "tipsdeservicio":
        $ruta="vistas/tipsdeservicio.php";
        break;

      case "nosotros":
        $ruta="vistas/nosotros.php";
        break;

      case "credicompra":
        $ruta="vistas/credicompra.php";
        break;

      default:
        $ruta="vistas/inicio.php";
        break;
	}
?>
