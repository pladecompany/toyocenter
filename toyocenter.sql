-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 03-04-2020 a las 02:59:41
-- Versión del servidor: 10.1.37-MariaDB
-- Versión de PHP: 7.0.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `toyocenter`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admins`
--

CREATE TABLE `admins` (
  `id` int(11) NOT NULL,
  `usuario` varchar(50) DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL,
  `correo` varchar(100) DEFAULT NULL,
  `acceso` int(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `admins`
--

INSERT INTO `admins` (`id`, `usuario`, `password`, `correo`, `acceso`) VALUES
(1, 'Administrador', '827ccb0eea8a706c4c34a16891f84e7b', 'admin@toyocenter.com', 0),
(2, 'Citas', '827ccb0eea8a706c4c34a16891f84e7b', 'citas@toyocenter.com', 1),
(3, 'Vehículos', '827ccb0eea8a706c4c34a16891f84e7b', 'vehiculos@toyocenter.com', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `agentes`
--

CREATE TABLE `agentes` (
  `id` int(11) NOT NULL,
  `cod_age` varchar(20) DEFAULT NULL,
  `nom_age` varchar(20) DEFAULT NULL,
  `ape_age` varchar(25) DEFAULT NULL,
  `tlf_age` varchar(15) DEFAULT NULL,
  `img_age` text,
  `est_age` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `agentes`
--

INSERT INTO `agentes` (`id`, `cod_age`, `nom_age`, `ape_age`, `tlf_age`, `img_age`, `est_age`) VALUES
(1, '00001', 'Asesor 3', '', '04120545738', 'https://exiauto.com/static/img/user.png', 1),
(2, '00002', 'Asesor 2', 'Toro', '04120545738', 'https://exiauto.com/static/img/user.png', 1),
(3, '00003', 'Asesor 1', '', '04120545738', 'https://exiauto.com/static/img/user.png', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `asignar_modelos_repuestos`
--

CREATE TABLE `asignar_modelos_repuestos` (
  `id` int(11) NOT NULL,
  `id_repuesto` int(11) DEFAULT NULL,
  `id_modelo` int(11) DEFAULT NULL,
  `anos` text,
  `anos_txt` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `asignar_modelos_repuestos`
--

INSERT INTO `asignar_modelos_repuestos` (`id`, `id_repuesto`, `id_modelo`, `anos`, `anos_txt`) VALUES
(18, 1, 1, '2015, 2020, 2002', NULL),
(19, 1, 3, '2010, 2011', NULL),
(41, 5, 3, '2015-2018', '2015,2016,2017,2018,'),
(42, 4, 1, '2000-2005,  2010, 2015-2020', '2000,2001,2002,2003,2004,2005,2010,2015,2016,2017,2018,2019,2020,'),
(43, 4, 5, '2000-2010, 2012', '2000,2001,2002,2003,2004,2005,2006,2007,2008,2009,2010,2012,'),
(44, 3, 3, '2002, 2003', '2002,2003,'),
(45, 3, 5, '2005, 2006', '2005,2006,'),
(46, 2, 1, '2001, 2002, 2003', '2001,2002,2003,'),
(47, 2, 3, '2001, 2002, 2003', '2001,2002,2003,');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categorias_repuestos`
--

CREATE TABLE `categorias_repuestos` (
  `id` int(11) NOT NULL,
  `nom_cat` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `categorias_repuestos`
--

INSERT INTO `categorias_repuestos` (`id`, `nom_cat`) VALUES
(1, 'Carrocería'),
(2, 'CAUCHOS'),
(3, 'FILTROS');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `citas`
--

CREATE TABLE `citas` (
  `id` int(11) NOT NULL,
  `id_usuario` int(11) DEFAULT NULL,
  `id_vehiculo` int(11) DEFAULT NULL,
  `id_agente` int(11) DEFAULT NULL,
  `motivo` text,
  `fecha` datetime DEFAULT NULL,
  `estatus` int(11) DEFAULT NULL,
  `fec_env` datetime DEFAULT NULL,
  `fac_res` datetime DEFAULT NULL,
  `tipo_cita` varchar(25) DEFAULT NULL,
  `id_kilometros` int(11) DEFAULT NULL,
  `id_falla` int(11) DEFAULT NULL,
  `observacion` text,
  `reprogramado` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comentarios_reclamos`
--

CREATE TABLE `comentarios_reclamos` (
  `id` int(11) NOT NULL,
  `id_reclamo` int(11) DEFAULT NULL,
  `id_usuario` int(11) DEFAULT NULL,
  `id_admin` int(11) DEFAULT NULL,
  `comentario` text,
  `fec_com` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `comentarios_reclamos`
--

INSERT INTO `comentarios_reclamos` (`id`, `id_reclamo`, `id_usuario`, `id_admin`, `comentario`, `fec_com`) VALUES
(17, 7, NULL, 1, 'Esta es otra prueba desde la compañía', '2020-01-21 14:18:14');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `departamentos`
--

CREATE TABLE `departamentos` (
  `id` int(11) NOT NULL,
  `nom_dep` varchar(50) DEFAULT NULL,
  `des_dep` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `departamentos`
--

INSERT INTO `departamentos` (`id`, `nom_dep`, `des_dep`) VALUES
(1, 'Vehículos', ''),
(2, 'Repuestos', ''),
(3, 'Servicios', ''),
(4, 'ELP', ''),
(5, 'Otros', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fallas`
--

CREATE TABLE `fallas` (
  `id` int(11) NOT NULL,
  `falla` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `fallas`
--

INSERT INTO `fallas` (`id`, `falla`) VALUES
(1, 'Revisión según Kilometraje (Diagnóstico)'),
(20, 'Alineación'),
(30, 'Balanceo'),
(40, 'Solo Cambio de Aceite y Filtro'),
(50, 'Revisión de Frenos'),
(60, 'Diagnóstico Tren Delantero'),
(70, 'Diagnóstico A/Acondicionado'),
(80, 'Diagnóstico Recalentamiento de Motor'),
(90, 'Revisión Falla de Encendido'),
(100, 'Limpieza de Inyectores'),
(110, 'Diagnóstico de Tripoides'),
(120, 'Diagnóstico de Amortiguadores'),
(130, 'Revisar Fuga de Aceite de Motor'),
(140, 'Revisar Ruido de Rodamiento'),
(150, 'Revisar Luz Encendida en el Tablero'),
(160, 'Revisión y Diagnóstico de Embrague'),
(170, 'Revisión y Diagnóstico de Refrigerante'),
(180, 'Revisar Alternador'),
(190, 'Revisar Arranque'),
(200, 'Revisión y Diagnóstico Ruido en terreno Terrenal'),
(201, 'No aplica');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `kilometrajes`
--

CREATE TABLE `kilometrajes` (
  `id` int(11) NOT NULL,
  `kilometros` varchar(100) DEFAULT NULL,
  `tipo_kil` varchar(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `kilometrajes`
--

INSERT INTO `kilometrajes` (`id`, `kilometros`, `tipo_kil`) VALUES
(1, 'No aplica', ''),
(2, '1.000 con garantía', 'gar'),
(3, '10.000 con garantía', 'gar'),
(4, '1.000 sin garantía', 'per'),
(5, '5.000 Kilometros', 'per'),
(60, '10.000 Kilometros', 'per'),
(65, '15.000 Kilometros', 'per'),
(70, '20.000 Kilometros', 'per'),
(75, '25.000 Kilometros', 'per'),
(80, '30.000 Kilometros', 'per'),
(85, '35.000 Kilometros', 'per'),
(90, '40.000 Kilometros', 'per'),
(95, '45.000 Kilometros', 'per'),
(100, '50.000 Kilometros', 'per'),
(105, '55.000 Kilometros', 'per'),
(110, '60.000 Kilometros', 'per'),
(115, '65.000 Kilómetros', 'per'),
(120, '70.000 Kilómetros', 'per'),
(125, '75.000 Kilómetros', 'per'),
(130, '80.000 Kilómetros', 'per'),
(135, '85.000 Kilómetros', 'per'),
(140, '90.000 Kilómetros', 'per'),
(145, '95.000 Kilómetros', 'per'),
(150, '100.000 Kilómetros', 'per'),
(155, '105.000 Kilómetros', 'per'),
(160, '110.000 Kilómetros', 'per'),
(165, '115.000 Kilómetros', 'per'),
(170, '120.000 Kilómetros', 'per'),
(175, '125.000 Kilómetros', 'per'),
(180, '130.000 Kilómetros', 'per'),
(185, '135.000 Kilómetros', 'per'),
(190, '140.000 Kilómetros', 'per'),
(195, '145.000 Kilómetros', 'per'),
(200, '150.000 Kilómetros', 'per'),
(205, '155.000 Kilómetros', 'per'),
(210, '160.000 Kilómetros', 'per'),
(215, '165.000 Kilómetros', 'per'),
(220, '170.000 Kilómetros', 'per'),
(225, '175.000 Kilómetros', 'per'),
(230, '180.000 Kilómetros o Superior', 'per');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `misvehiculos`
--

CREATE TABLE `misvehiculos` (
  `id` int(11) NOT NULL,
  `id_usuario` int(11) DEFAULT NULL,
  `id_modelo` int(11) DEFAULT NULL,
  `placa` varchar(20) DEFAULT NULL,
  `serial1` varchar(40) DEFAULT NULL,
  `serial2` varchar(40) DEFAULT NULL,
  `serial3` varchar(40) DEFAULT NULL,
  `serial4` varchar(40) DEFAULT NULL,
  `ano` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `misvehiculos`
--

INSERT INTO `misvehiculos` (`id`, `id_usuario`, `id_modelo`, `placa`, `serial1`, `serial2`, `serial3`, `serial4`, `ano`) VALUES
(3, 1, 1, '0129312309', '123456789012345', 'azul', 'Automatico', '', 2000),
(4, 1, 1, '00012312', '129319239812312893128', 'verde', 'Automatico', '', 2000),
(5, 5, 2, 'jjwwee', '0123912391238123', NULL, NULL, NULL, 2001),
(6, 7, 6, 'AB258WN', '8AJHU3FS7K0360093', 'Blanco', 'Automatico', NULL, 2019),
(7, 7, 3, 'AF520TG', '8XBBA42E7DR825844', 'Azul', 'Automatico', NULL, 2013),
(8, 9, 3, 'AD754UA', '8XBBA42E3A7809633', 'GRIS', 'Automatico', NULL, 2010),
(9, 12, 1, '4654654654654', '457ase896734rfd', 'azul', 'Automatico', NULL, 2020),
(10, 13, 6, 'AB975MT', '8XAYU59G5KR001762', 'BLANCO', 'Automatico', NULL, 2019),
(11, 15, 2, 'MBF69B', '123456789012345', 'BEIGE', 'Automatico', NULL, 1998),
(12, 1, 1, 'MFL63A', '123456789012345', 'GRIS', 'Automatico', NULL, 2007),
(13, 6, 1, 'AA123AA', 'Jtdw13479j1438298', 'Azul ', 'Automatico', NULL, 2019),
(14, 22, 3, 'AG256HG', '8XBBA42E8DR828090', 'Purpura', 'Automatico', NULL, 2013),
(15, 25, 3, 'AH578MV', '8XBBA3HE8JR002434', 'BLANCO', 'Automatico', NULL, 2018),
(16, 29, 8, 'TAS01W', '9FH11UJ9089019397', 'GRIS', 'Manual', NULL, 2008),
(17, 32, 1, 'GDO41B', 'JTDKW923372007302', 'Negro', 'Automatico', NULL, 2007),
(18, 33, 1, 'AB592MM', 'JTDKW923475072612', 'azul metalico', 'Automatico', NULL, 2007),
(19, 40, 3, 'AA272ZM', '8xbba42e997802018', 'beige', 'Manual', NULL, 2009),
(20, 41, 1, 'AA799MW', 'JTDJW923X75036127', 'Negro', 'Automatico', NULL, 2007),
(21, 43, 3, 'AC866SE', '8XBBA3HE1HR002334', 'PLATA GALAXY', 'Automatico', NULL, 2017);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `modelos`
--

CREATE TABLE `modelos` (
  `id` int(11) NOT NULL,
  `modelo` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `modelos`
--

INSERT INTO `modelos` (`id`, `modelo`) VALUES
(1, 'COROLLA'),
(2, 'CAMRY'),
(5, 'Yaris'),
(6, 'Fortuner'),
(7, 'hiace'),
(8, 'Hilux'),
(10, 'LAND CRUISER');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `modelos_citas`
--

CREATE TABLE `modelos_citas` (
  `id` int(11) NOT NULL,
  `modelo` text,
  `transmision` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `modelos_citas`
--

INSERT INTO `modelos_citas` (`id`, `modelo`, `transmision`) VALUES
(1, 'Yaris', 'Automatica'),
(2, 'Camry', 'Manual'),
(3, 'Corolla', 'Automatica'),
(5, '4Runner', 'Automatica'),
(6, 'Fortuner', 'Automática'),
(7, 'Hiace', NULL),
(8, 'Hilux', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `modelos_repuestos`
--

CREATE TABLE `modelos_repuestos` (
  `id` int(11) NOT NULL,
  `modelo` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `modelos_repuestos`
--

INSERT INTO `modelos_repuestos` (`id`, `modelo`) VALUES
(1, 'Modelo 1-1'),
(2, 'Modelo 2'),
(3, 'Modelo 3'),
(5, 'Modelo 5'),
(6, 'modelo testd');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `noticias`
--

CREATE TABLE `noticias` (
  `id` int(11) NOT NULL,
  `titulo` varchar(100) DEFAULT NULL,
  `descripcion` longtext,
  `img` text,
  `fec_reg_noti` date DEFAULT NULL,
  `est_noti` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `noticias`
--

INSERT INTO `noticias` (`id`, `titulo`, `descripcion`, `img`, `fec_reg_noti`, `est_noti`) VALUES
(8, 'El Toyota Yaris Hatchback 2020 para Japón obtiene AWD, plataforma completamente nueva', '\r\nEl Toyota Yaris est&aacute; muerto en Estados Unidos, pero vivir&aacute; en todo el resto del mundo. Todav&iacute;a obtenemos la versi&oacute;n angular, basada en Mazda, que est&aacute; identificada como Toyota, pero esta nueva que obtendr&aacute; el resto del mundo es un poco m&aacute;s completa y casi completamente diferente. El nuevo Yaris en el extranjero se basa en la plataforma de Nueva Arquitectura Global (TNGA) de Toyota y cuenta con tres opciones de tren motriz, mientras que la versi&oacute;n que tenemos en Estados Unidos solo tiene una.\r\nLa plataforma TNGA se usa por primera vez en un autom&oacute;vil subcompacto, y Toyota dice que a partir de ahora basar&aacute;n todos sus autom&oacute;viles subcompactos en esta plataforma. El uso de esta plataforma ayuda a darle al Yaris un poco m&aacute;s de espacio en la cabina junto con una suspensi&oacute;n actualizada y un cuerpo que es 100 libras m&aacute;s ligero.\r\n \r\nEl 1.5 litros de tres cilindros est&aacute; disponible con un nuevo sistema h&iacute;brido que proviene de los m&aacute;s grandes que se encuentran en el Corolla, RAV4 y Camry. Se ofrece con el sistema el&eacute;ctrico de tracci&oacute;n total E-Four de Toyota, otra primicia para los autos subcompactos de Toyota. Toyota a&uacute;n no ha reclamado los n&uacute;meros de potencia, pero s&iacute; dice que el h&iacute;brido tiene un 15 por ciento m&aacute;s de potencia combinada y un aumento de m&aacute;s del 20 por ciento en la eficiencia del combustible. Tambi&eacute;n hay disponible una versi&oacute;n a gasolina del motor de 1.5 litros y uno de tres cilindros de 1.0 litro. En los Estados Unidos, el sed&aacute;n Yaris con sede en Mazda viene de serie con una transmisi&oacute;n manual, aunque el hatchback solo est&aacute; disponible con una transmisi&oacute;n autom&aacute;tica de seis velocidades. En la nueva escotilla Yaris que no obtendremos, se ofrece un manual de seis velocidades con el motor de gasolina de 1.5 litros. Una transmisi&oacute;n continua de cambio continuo con tracci&oacute;n delantera o tracci&oacute;n total est&aacute; disponible para el 1.5 litros, y un CVT con tracci&oacute;n delantera est&aacute; disponible para el 1.0 litro.\r\nLa bater&iacute;a de hidruro de n&iacute;quel-metal que se encontraba en el Yaris de anta&ntilde;o ha sido reemplazada por una bater&iacute;a h&iacute;brida de iones de litio, que seg&uacute;n Toyota puede ayudar a aumentar la aceleraci&oacute;n. La nueva bater&iacute;a tambi&eacute;n es un 27 por ciento m&aacute;s ligera que la que reemplaza.\r\nToyota Safety Sense es est&aacute;ndar en el nuevo Yaris, presentando el debut de un sistema de asistencia de estacionamiento, Advanced Park, que es nuevo para Toyota, y un sistema de detecci&oacute;n de autos que se aproxima, otro primero; Tambi&eacute;n incluye las caracter&iacute;sticas de seguridad activa esperadas, como control de crucero adaptativo y asistencia de mantenimiento de carril. Una pantalla frontal de color de 10 pulgadas tambi&eacute;n es est&aacute;ndar.\r\nEl Toyota Yaris 2020 estar&aacute; en exhibici&oacute;n en el Auto Show de Tokio esta semana y a la venta en Jap&oacute;n a partir de mediados de febrero de 2020.\r\n\r\n', 'https://exiauto.com/static/img/files/2019_10_24_11_28_14toyota2.jpg', '2019-10-24', 1),
(9, 'Procedimientos que debes realizar con profesionales como Toyomaya, y no por tu cuenta. ', 'Hay procedimientos que pueden observarse como sencillos para el usuario, pero por su real dificultad o consecuencias que puede desencadenar en tu veh&iacute;culo, te recomendamos hacerlas con un profesional:\r\n\r\n1. Cambiar el aceite y su filtro: En esta operaci&oacute;n se observa como muy simple para los usuarios, sin embargo hay que tener en cuenta el aceite adecuado para tu veh&iacute;culo, el estatus, y la gesti&oacute;n de los residuos como marca la ley.\r\n\r\n2. Cambiar las pastillas y discos de freno: Hay que tener en cuenta que los frenos son unos de los elementos de seguridad m&aacute;s importantes del vehiculo, por eso este cambio debe realizarlo un profesional. Adem&aacute;s de los conocimientos, hay que tener todos las herramientas necesarias.\r\n\r\n3. Cambiar la distribuci&oacute;n (correa o kit) y la bomba de agua: Se trata de una operaci&oacute;n m&aacute;s compleja, que de no realizarse adecuadamente, puede da&ntilde;ar el motor de tu veh&iacute;culo.\r\n\r\n4. Recargar el aire acondicionado:  Existen varios tipos de gas y utilizar uno que no sea el adecuado puede resultar peligroso. Hay que tener en cuenta que el gas es inflamable y su manipulaci&oacute;n es muy peligrosa.\r\n\r\n5. Cambiar l&iacute;quido de frenos: Es una tarea que puede resultar complicada y que un profesional puede realizar sin problemas.', 'https://exiauto.com/static/img/files/2019_11_26_09_56_07toyota_VOLANTE.jpg', '2019-11-26', 1),
(10, '¿Conoces la nueva Toyota Hilux GR Sport?', 'Toyota presenta la Hilux GR Sport, una nueva versi&oacute;n de Toyota GAZOO Racing.\r\nLa nueva Hilux GR Sport suma tambi&eacute;n equipamiento funcional pensado para brindarle una experiencia superadora al conductor off-road.\r\nEquipada con un motor V6 naftero de 238 CV de potencia m&aacute;xima, una caja autom&aacute;tica de 6 velocidades.\r\nEste conjunto entrega una aceleraci&oacute;n m&aacute;s en&eacute;rgica, con reacciones m&aacute;s r&aacute;pidas, brind&aacute;ndole al veh&iacute;culo un acentuado car&aacute;cter deportivo que resulta en una conducci&oacute;n m&aacute;s emocionante.\r\n', 'https://exiauto.com/static/img/files/2020_02_04_13_55_34gr_sport-3.jpg', '2020-02-04', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reclamos`
--

CREATE TABLE `reclamos` (
  `id` int(11) NOT NULL,
  `id_usuario` int(11) DEFAULT NULL,
  `fec_reg_rec` datetime DEFAULT NULL,
  `mensaje` text,
  `est_rec` int(1) DEFAULT NULL,
  `tipo` varchar(1) DEFAULT NULL,
  `id_departamento` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `reclamos`
--

INSERT INTO `reclamos` (`id`, `id_usuario`, `fec_reg_rec`, `mensaje`, `est_rec`, `tipo`, `id_departamento`) VALUES
(7, 15, '2020-01-21 14:14:50', 'Prueba de sugerencia desde los desarrolladores', 1, 'S', 5),
(8, 6, '2020-01-27 08:18:44', 'MI VEHICULO QUEDO SUCIO ', 1, 'R', 3),
(10, 1, '2020-03-17 17:49:12', 'Este repuesto esta ok.', 0, 'R', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `repuestos`
--

CREATE TABLE `repuestos` (
  `id` int(11) NOT NULL,
  `id_categoria` int(11) DEFAULT NULL,
  `nom_rep` varchar(100) DEFAULT NULL,
  `des_rep` text,
  `ano_rep` int(11) DEFAULT NULL,
  `pre_rep` float DEFAULT NULL,
  `cam_1` varchar(100) DEFAULT NULL,
  `cam_2` varchar(100) DEFAULT NULL,
  `cam_3` varchar(100) DEFAULT NULL,
  `img1` text,
  `img2` text,
  `img3` text,
  `img4` text,
  `img5` text,
  `est_rep` int(1) DEFAULT NULL,
  `cod_rep` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `repuestos`
--

INSERT INTO `repuestos` (`id`, `id_categoria`, `nom_rep`, `des_rep`, `ano_rep`, `pre_rep`, `cam_1`, `cam_2`, `cam_3`, `img1`, `img2`, `img3`, `img4`, `img5`, `est_rep`, `cod_rep`) VALUES
(1, 3, 'Filtro de aceite', 'Filtro de aceite Filtro de aceiteFiltro de aceite Filtro de aceiteFiltro de aceite Filtro de aceiteFiltro de aceite Filtro de aceiteFiltro de aceite Filtro de aceiteFiltro de aceite Filtro de aceiteFiltro de aceite Filtro de aceiteFiltro de aceite Filtro de aceiteFiltro de aceite Filtro de aceiteFiltro de aceite Filtro de aceiteFiltro de aceite Filtro de aceite\r\n\r\nFiltro de aceite Filtro de aceiteFiltro de aceite Filtro de aceite\r\n\r\n\r\nFiltro de aceite Filtro de aceiteFiltro de aceite Filtro de aceiteFiltro de aceite Filtro de aceite', NULL, 100000, NULL, NULL, NULL, 'http://localhost/toyomaya.com/static/img/files/2020_03_11_20_57_04índice.jpg', NULL, NULL, NULL, NULL, 1, NULL),
(2, 3, 'Filtro de aceite FIAT', 'Filtro de aceite FIAT', NULL, 30, NULL, NULL, NULL, 'http://localhost/toyomaya.com/static/img/files/2020_03_17_14_46_171.jpg', 'http://localhost/toyomaya.com/static/img/files/2020_03_17_16_45_342.jpg', 'http://localhost/toyomaya.com/static/img/files/2020_03_17_16_45_343.jpg', NULL, NULL, 1, '000004'),
(3, 2, 'SWITCH DE TEMPERATURA', 'Testing testing testing testingTesting testing testing testingTesting testing testing testingTesting testing testing testing\r\n\r\nTesting testing testing testingTesting testing testing testing\r\n\r\nTesting testing testing testingTesting testing testing testingTesting testing testing testingTesting testing testing testing\r\n', NULL, 10, NULL, NULL, NULL, 'http://localhost/toyomaya.com/static/img/files/2020_03_18_00_16_334.jpg', NULL, NULL, NULL, NULL, 1, '000003'),
(4, 1, 'testing', 'testing.jkbkjhkhkk', NULL, 921030, NULL, NULL, NULL, 'https://toyomaya.com/static/img/files/2020_03_21_11_51_32Captura de pantalla 2020-03-19 a la(s) 10.16.15 a. m..png', NULL, NULL, NULL, NULL, 1, '000002'),
(5, 2, 'alskdjas', 'as,dmasjd aslkdadas', NULL, 2999, NULL, NULL, NULL, 'https://toyomayamaracay.com/static/img/files/2020_03_26_14_48_12JonLack.jpg', NULL, NULL, NULL, NULL, 1, '000001');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `solicitud_repuestos`
--

CREATE TABLE `solicitud_repuestos` (
  `id` int(11) NOT NULL,
  `nom` varchar(30) DEFAULT NULL,
  `ape` varchar(30) DEFAULT NULL,
  `tel` varchar(15) DEFAULT NULL,
  `cor` varchar(100) DEFAULT NULL,
  `repuesto` text,
  `fec_reg_sol` datetime DEFAULT NULL,
  `estatus` int(1) DEFAULT NULL,
  `fec_pro_sol` datetime DEFAULT NULL,
  `id_repuesto` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `solicitud_repuestos`
--

INSERT INTO `solicitud_repuestos` (`id`, `nom`, `ape`, `tel`, `cor`, `repuesto`, `fec_reg_sol`, `estatus`, `fec_pro_sol`, `id_repuesto`) VALUES
(1, 'carlos', 'castellanos', '04120545738', 'castellanos@pladecompany.com', 'Esta es una prueba de las solicitudes de repuestos desde la pagina web.', '2020-02-18 16:32:08', 1, NULL, NULL),
(2, 'Andrey ', 'Colmenares ', '04125747451 ', 'andrycolmenares24@hotmail.com ', 'Espirales delantero y trasero de una hilux doble cabina 2.7 año 2007 ', '2020-02-19 11:45:24', 1, NULL, NULL),
(3, 'Alys', 'Alfonzo ', '04148080344', 'alfonzoalys@gmail.com ', 'Control de mando eleva vidrios electrónico de puerta de piloto Yaris Belta año 2007 ', '2020-02-19 14:37:56', 1, NULL, NULL),
(4, 'Alys', 'Alfonzo ', '04148080344', 'alfonzoalys@gmail.com ', 'Control de mando eleva vidrios electrónico de puerta de piloto Yaris Belta año 2007 ', '2020-02-19 14:38:04', 1, NULL, NULL),
(5, 'RAFAEL', 'ALVAREZ', '04122862541', 'info@espacioconvalor.com', 'REGULADOR DE ALTERNADOR 4 PINES PARA YARIS 3 PUERTAS 2007 ', '2020-02-20 14:08:27', 1, NULL, NULL),
(6, 'JOSEFINA ', 'SANCHEZ ', '04142209965', 'josefinasanchezoyuela@gmail.com', 'El flotante de la Bomba de Gasolina , Camry 1998', '2020-02-20 15:30:35', 1, NULL, NULL),
(7, 'Daniela', 'Egui', '04241760238', 'eguirubiodaniela@yahoo.com.ve', 'Filtros de aceite, gasolina y aire para Toyota Corolla 2010', '2020-02-26 09:00:38', 1, NULL, NULL),
(8, 'celimar', 'moreno', '04141357245', 'celimarmccs@gmail.com', 'buenas tards por favor deseo saber si tiene anillos de baiby canry carburado motor 1.6 a 0.40\n', '2020-02-26 12:24:38', 1, NULL, NULL),
(9, 'celimar', 'moreno', '04141357245', 'celimarmccs@gmail.com', 'buenas tards por favor deseo saber si tiene anillos de baiby canry carburado motor 1.6 a 0.40\n', '2020-02-26 12:24:38', 1, NULL, NULL),
(10, 'celimar', 'moreno', '04141357245', 'celimarmccs@gmail.com', 'buenas tards por favor deseo saber si tiene anillos de baiby canry carburado motor 1.6 a 0.40\n', '2020-02-26 12:24:38', 1, NULL, NULL),
(11, 'celimar', 'moreno', '04141357245', 'celimarmccs@gmail.com', 'buenas tards por favor deseo saber si tiene anillos de baiby canry carburado motor 1.6 a 0.40\n', '2020-02-26 12:24:38', 1, NULL, NULL),
(12, 'celimar', 'moreno', '04141357245', 'celimarmccs@gmail.com', 'buenas tards por favor deseo saber si tiene anillos de baiby canry carburado motor 1.6 a 0.40\n', '2020-02-26 12:24:38', 1, NULL, NULL),
(13, 'Rafael', 'Toro', '04149213982', 'tororafael@gmail.com', 'Para previa 2009. NO Smart\nBases de motor y caja. Todas.\nCorrea Unica y tensor.\nEstopera trasera de sigueñal.\nFiltro Aceite.', '2020-02-27 08:54:04', 1, NULL, NULL),
(14, 'Fabian', 'Alvarez', '04143226012', 'fabian.alvarez@aig-ca.com.ve', 'Solicitó la rolinera de aguja del transfer para hilux 2.7 año 2008, 2.7 4x4', '2020-02-27 09:59:46', 1, NULL, NULL),
(15, 'franklin ', 'rivero', '04141876276', 'frgofc@gmail.com', 'buenos días \nando buscando las 4 gomas de los tripoides para un toyota corolla 2009 GLI si me podrían ayudar sería buenísimo y muchas gracias ', '2020-02-28 09:23:42', 1, NULL, NULL),
(16, 'José  Alejan', 'Pinto Arno', '04242281302', 'Josepintoa@gmail.com', 'Guía trasera izquierda, del parachoque de una 4 Runner, 2008', '2020-02-28 11:07:50', 1, NULL, NULL),
(17, 'Omar ', 'rios ', '0412 8779734', 'Omarsk_8@hotmail.com', 'kit cadena de tiempo yaris belta motor 1.5', '2020-02-28 14:22:42', 1, NULL, NULL),
(18, 'carlos ', 'castellanso', '04120545738', 'castellanos@pladecompany.com', 'ksadj asldkasj dlas dlsajdlas', '2020-03-17 16:24:00', 1, NULL, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL,
  `ced_usu` varchar(15) DEFAULT NULL,
  `nom_usu` varchar(30) DEFAULT NULL,
  `ape_usu` varchar(35) DEFAULT NULL,
  `cor_usu` varchar(100) DEFAULT NULL,
  `tel_usu` varchar(15) DEFAULT NULL,
  `pas_usu` varchar(500) DEFAULT NULL,
  `fec_nac_usu` date DEFAULT NULL,
  `fec_reg_usu` datetime DEFAULT NULL,
  `emp_usu` varchar(100) DEFAULT NULL,
  `facebook` varchar(40) DEFAULT NULL,
  `instagram` varchar(40) DEFAULT NULL,
  `twitter` varchar(40) DEFAULT NULL,
  `img_usu` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `ced_usu`, `nom_usu`, `ape_usu`, `cor_usu`, `tel_usu`, `pas_usu`, `fec_nac_usu`, `fec_reg_usu`, `emp_usu`, `facebook`, `instagram`, `twitter`, `img_usu`) VALUES
(1, 'V-21024060', 'Carlos', 'Castellanos', 'castellanoscarlos15@gmail.com', '04120545738', '12345', '1993-10-15', '2019-12-23 13:50:45', '', 'casteca15', 'casteca15', 'casteca15', 'https://pladecompany.com/demos/toyomayamaracay.com/static/img/files/2020_03_14_13_38_10cc-1.jpg'),
(2, 'J-40087772', 'Ernesto', 'Fernandez', 'contacto@pladecompany.com', '04120545738', '12345', '1993-10-15', '2019-10-07 12:08:14', 'Plade Company', NULL, NULL, NULL, NULL),
(5, 'V-21160654', 'Daniela1', 'Manzanilla1', 'd1ani@gmail.com', '04123123123121', '12345', '1992-10-15', '2019-10-09 22:04:59', 'Plade1', NULL, NULL, NULL, NULL),
(6, 'V-24211288', 'giselle', 'cedeño', 'gcedeno@exiauto.com', '04120983189', '12397081', NULL, '2019-11-15 13:25:29', NULL, NULL, NULL, NULL, NULL),
(7, 'J-411673528', 'Miguel', 'Martínez', 'mmcastro76@gmail.com', '04143383179', 'mmc45162048f', NULL, '2019-11-18 08:46:56', 'Corporación Alekami, C. A.', NULL, NULL, NULL, NULL),
(8, 'V-12916924', 'Edicza Coromoto', 'Gonzalez Alvarez', 'gonzalezedicza59@gmail.com', '04142314762', 'diegoyyo2325', NULL, '2019-11-20 12:12:12', NULL, NULL, NULL, NULL, NULL),
(9, 'V-4275101', 'Susana', 'Belluardo', 'belluardosusana@gmail.com', '04142350506', '18081953', NULL, '2019-12-04 07:22:01', NULL, NULL, NULL, NULL, NULL),
(10, 'V-20077600', 'Juan ', 'Escalante', 'escalantejuan3592@gmail.com', '04242249892', '22042008', NULL, '2019-12-04 13:27:39', NULL, NULL, NULL, NULL, NULL),
(11, 'V-3720061', 'Elizabeth ', 'Cedeño ', 'elizabeth@fanjet.com', '04142005578', 'beth67.ec', NULL, '2019-12-05 07:54:13', NULL, NULL, NULL, NULL, NULL),
(12, 'V-14786421', 'carlos ', 'camacho ', 'carloscamachorivero@hotmail.com', '04143994798', '248605', NULL, '2019-12-05 10:56:19', NULL, NULL, NULL, NULL, NULL),
(13, 'V-15055273', 'Jose de los Santos', 'Oyarbe Ramoa', 'jose.oyarbe@gmail.com', '04241162838', 'Entrust12.', NULL, '2019-12-16 10:09:32', NULL, NULL, NULL, NULL, NULL),
(14, 'V-18027244-1', 'CESAR', 'MUÑOZ', 'cjmrios@gmail.com', '04142416486', 'cm135792468', NULL, '2019-12-18 11:45:24', NULL, NULL, NULL, NULL, NULL),
(15, 'V-11886187', 'miguel', 'farias', 'miguel_farias@hotmail.com', '04122680712', 'migfar', NULL, '2019-12-18 12:52:33', NULL, NULL, NULL, NULL, NULL),
(19, 'V-13972999', 'freddy johan', 'sanchez ramirez ', 'freddyjohan35@hotmail.com', '04166781773', '13972999/cordero', NULL, '2020-01-17 10:47:50', NULL, NULL, NULL, NULL, NULL),
(20, 'V-3789052', 'Beltran', 'Contreras', 'jorge.contreras@circuitolider.com', '04143741309', 'lider92', NULL, '2020-01-20 08:43:22', NULL, NULL, NULL, NULL, NULL),
(21, 'V-25716493', 'Ariagna', 'Pamelá', 'a.ariagna.p@gmail.com', '04129803941', '123456a', NULL, '2020-01-20 09:30:57', NULL, NULL, NULL, NULL, NULL),
(22, 'V-10337502', 'Rogelio', 'Carrillo', 'r.carrillo@outlook.com', '04143234138', '3S4loc8DhW7T', NULL, '2020-01-20 12:15:44', NULL, NULL, NULL, NULL, NULL),
(24, 'V-14989770', 'ramon', 'iglesias', 'riglesias47@gmail.com', '04122772982', '14989770', NULL, '2020-01-20 14:28:47', NULL, NULL, NULL, NULL, NULL),
(25, 'V-19371327', 'Barbara', 'Bautista Morales', 'blbautistam@gmail.com', '04149107439', 'anacleta232', NULL, '2020-01-21 09:45:21', NULL, NULL, NULL, NULL, NULL),
(26, 'V-7092221', 'CLARA LUCIA', 'VENTO MENDOZA', 'claraluciavento@gmail.com', '04166244745', 'KYRO2019', '1967-02-20', '2020-01-24 08:32:25', '', '', '@claraluciavento', '', NULL),
(27, 'V-3859941', 'edgar ', 'naranjo', 'enaranjo.tranred@gmail.com', '04141139773', 'aleeli01', NULL, '2020-01-24 09:47:48', NULL, NULL, NULL, NULL, NULL),
(29, 'V-10542478', 'Jesus', 'Salazar', 'chui.x3@gmail.com', '04166080004', 'Calabaza1', NULL, '2020-01-24 10:41:53', NULL, 'chuix3', 'chui.x3', '@jsalazarx01', NULL),
(30, 'V-9882633', 'gretchen', 'lauria', 'gretchenlauria@hotmail.com', '04143246260', '031269', NULL, '2020-01-29 15:41:27', NULL, NULL, NULL, NULL, NULL),
(31, 'V-6557658', 'GLADYS', 'TENORIO VALERA', 'GLADYSTENORIOV@GMAIL.COM', '04122638249', '12311121516', NULL, '2020-02-05 16:18:04', NULL, NULL, NULL, NULL, NULL),
(32, 'V-2091177', 'Maria Josefina', 'Gonzalez de Argento', 'mjargento@gmail.com', '04242002520', 'westminster20', NULL, '2020-02-06 12:31:46', NULL, NULL, NULL, NULL, NULL),
(33, 'V-4120287', 'Elizabeth', 'Rodriguez', 'eliro91@gmail.com', '04122156281', 'laguna8691', NULL, '2020-02-13 12:13:29', NULL, NULL, NULL, NULL, NULL),
(34, 'V-5003059', 'Marcos', 'Schnee', 'schneemarcos@gmail.com', '04241716352', 'mda67v', NULL, '2020-02-18 10:41:13', NULL, NULL, NULL, NULL, NULL),
(35, 'V-3176946', 'Carlos ', 'Cosson', 'luiscosson@gmail.com', '04166209666', 'abril2020', NULL, '2020-02-19 11:14:58', NULL, NULL, NULL, NULL, NULL),
(36, 'V-6867409', 'JUAN CARLOS', 'PARDO PISANI', 'jcpp2000@yahoo.com', '04241775803', 'fabi2403', NULL, '2020-02-20 08:53:07', NULL, NULL, NULL, NULL, NULL),
(37, 'V-12410746', 'robert', 'alvarez', 'roberthalvarez16@gmail.com', '04141349437', 'rob12410746', NULL, '2020-02-20 22:21:09', NULL, 'Roberthalvarez@hotmail.com', NULL, NULL, NULL),
(38, 'J-080271084', 'Jose', 'Diaz', 'jdiaz@disteinsa.com', '04148232980', 'disteinsa', NULL, '2020-02-26 10:09:42', 'Disteinsa, C.A.', NULL, NULL, NULL, NULL),
(39, 'V-6346955', 'Vicente', 'Adriani', 'vadriani10@gmail.com', '04241222827', '21267303', NULL, '2020-02-26 10:52:23', NULL, NULL, NULL, NULL, NULL),
(40, 'V-12544268', 'Azael', 'Camacho', 'azaelcamacho66@gmail.com', '04141092840', 'ac12544268', NULL, '2020-02-26 11:28:03', NULL, NULL, NULL, NULL, NULL),
(41, 'V-18954419', 'Karina', 'Licitra', 'karinalicitra@gmail.com', '04122244780', 'panini', NULL, '2020-02-27 10:23:40', NULL, NULL, NULL, NULL, NULL),
(42, 'V-3187305', 'Vicente', 'Antonorsi', 'administracion@anilarquitectura.com', '04142462560', '3187305', NULL, '2020-02-28 08:55:28', NULL, NULL, NULL, NULL, NULL),
(43, 'V-16007178', 'ALFREDO', 'MACHADO', 'machado.krueger@gmail.com', '04242489792', 'Aemk2308', NULL, '2020-02-29 11:56:39', NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vehiculos`
--

CREATE TABLE `vehiculos` (
  `id` int(11) NOT NULL,
  `id_modelo` int(11) DEFAULT NULL,
  `nom_veh` varchar(100) DEFAULT NULL,
  `ano_veh` int(11) DEFAULT NULL,
  `pre_veh` float DEFAULT NULL,
  `tra_veh` varchar(20) DEFAULT NULL,
  `est_veh` int(11) DEFAULT NULL,
  `imp_veh` varchar(1) DEFAULT NULL,
  `sto_veh` int(11) DEFAULT NULL,
  `des_veh` text,
  `fle_veh` text,
  `dis_veh` text,
  `seg_veh` text,
  `img1` text,
  `img2` text,
  `img3` text,
  `img4` text,
  `img5` text,
  `doc` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `vehiculos`
--

INSERT INTO `vehiculos` (`id`, `id_modelo`, `nom_veh`, `ano_veh`, `pre_veh`, `tra_veh`, `est_veh`, `imp_veh`, `sto_veh`, `des_veh`, `fle_veh`, `dis_veh`, `seg_veh`, `img1`, `img2`, `img3`, `img4`, `img5`, `doc`) VALUES
(8, 7, 'Hiace Panel ', 2019, 0, 'Manual', 1, '1', 0, 'La Toyota Hiace diseñada para emprendedores que necesitan una máquina de carga perfecta para los objetivos de su negocio, sumamente versátil y de una alta funcionalidad.\r\n\r\n Déjate sorprender con la nueva Hiace, un espacio tan grande como tus sueños.', 'Toyota Hiace posee una excelente maniobrabilidad gracias a su reducido radio de giro (5.0 mts. para techo bajo, 6.2 mts. para techo alto).\r\n', '• Asientos delanteros reclinables tipo butaca con apoya cabeza \r\n• Faros de Halógeno \r\n• Tercer Stop (Tipo Led)\r\n• Múltiples compartimientos\r\n• Tapicería de tela Gris \r\n• Portavasos \r\n• Sistema de Audio AM/FM con entrada USB \r\n• Aire Acondicionado ', '• Cinturones de seguridad delanteros de 3 puntos (pasajero y co-piloto) \r\n• Cinturones de seguridad delantero de 2 puntos (central) \r\n• Estructura de carrocería rígida amortiguada contra impactos\r\n• Volante basculante y columna de dirección colapsable en caso de impacto\r\n• Barras de seguridad en las puertas', 'https://exiauto.com/static/img/files/2019_09_12_09_32_11hiace blanca cerrada frente-min.PNG', 'https://exiauto.com/static/img/files/2019_09_12_09_32_11hiace lado blanca cerrada}-min.PNG', 'https://exiauto.com/static/img/files/2019_09_12_09_32_11panel beige and brown dos puestos-min.PNG', 'https://exiauto.com/static/img/files/2019_09_12_09_32_11espalda desde afuera con vacia llena-min.PNG', 'https://exiauto.com/static/img/files/2019_09_12_09_32_12interior vacio dos puestos-min.PNG', NULL),
(10, 2, 'Camry', 2019, NULL, 'Automático', 1, '1', 0, 'Lujo y confort a simple vista\r\n', NULL, 'El nuevo Camry es la síntesis perfecta entre moderno, deportivo y elegante. El renovador frente realza su presencia a partir del diseño de las ópticas y de la parrilla inferior. Las llantas de 17” remarcan un perfil más deportivo y audaz.\r\n\r\nSu renovador interior ofrece asientos tapizados en cuero color negro y detalles similar a la madera. El audio con pantalla táctil de 8” y el display de información múltiple de 7” pueden controlarse desde el volante.', 'Además de tener una carrocería de seguridad para impactos con la clasificación global más alta, el Camry está repleto de funciones avanzadas en seguridad activa. Por si fuera poco, también está equipado con una serie de medidas de seguridad pasiva: airbags frontales, laterales, de cortina y de rodilla para proteger la integridad de los ocupantes ante la ocasión de un impacto.\r\n\r\nEl control de estabilidad (vsc) ayudará a mantener todo en orden ante situaciones adversas. El Camry cuenta con discos de 17” en las 4 ruedas, además de sistema de anti-bloqueo ABS y BA (BrakeAssist) que brindan la mayor seguridad y potencia de frenado.\r\n\r\n Airbags frontales, laterales, de cortina y de rodilla para conductor, para proteger la integridad de los ocupantes ante la ocasión de un impacto.', 'https://exiauto.com/static/img/files/2019_09_12_09_01_01camry calle.jpg', 'https://exiauto.com/static/img/files/2019_09_12_09_01_01camry frente.PNG', NULL, 'https://exiauto.com/static/img/files/2019_09_12_09_01_01espalda lado camry.PNG', 'https://exiauto.com/static/img/files/2019_09_12_09_01_02camry espalda.PNG', NULL),
(11, 7, 'Hiace Commuter', 2019, 0, 'Manual', 1, '1', 0, '', '', '', '', 'https://exiauto.com/static/img/files/2019_09_12_09_42_44hiace commuter frente-min.jpg', 'https://exiauto.com/static/img/files/2019_09_12_09_42_45hiace lado commuter-min.PNG', 'https://exiauto.com/static/img/files/2019_09_12_09_42_45hiace commuter espalda-min.PNG', 'https://exiauto.com/static/img/files/2019_09_12_09_42_45interior puestos-min.PNG', 'https://exiauto.com/static/img/files/2019_09_12_09_42_46panel 3 puestos-min.PNG', NULL),
(12, 6, 'Fortuner Nacional', 2019, 0, 'Manual', 1, '0', 0, '', '', '', '', 'https://exiauto.com/static/img/files/2019_09_12_09_49_54FRENTE FORTUNER NACIONAL-min.jpg', 'https://exiauto.com/static/img/files/2019_09_12_09_49_54LADO FORTUNER NACIONAL-min.jpg', 'https://exiauto.com/static/img/files/2019_09_12_09_49_55ESPALDA FORTUNER NACIONAL-min.jpg', 'https://exiauto.com/static/img/files/2019_09_12_09_49_55TABLERO FORTUNER-min.jpg', 'https://exiauto.com/static/img/files/2019_09_12_09_49_55ASIENTO FORTUNER-min.jpg', NULL),
(14, 10, 'Land Cruiser Prado VX', 2020, 0, 'Automático', 1, '1', 0, 'Con un aspecto fuerte y un alto nivel de calidad, durabilidad y confiabilidad, que alcanza los estándares más altos de diseño e innovación', 'Excelente maniobrabilidad', 'Diseño “modernidad inteligente” que da un carácter elegante y robusto, espacio interior más amplio', 'El radar detecta los vehículos que circulan en el siguiente carril. Cuando el vehículo ingresa al área del punto ciego, el indicador LED montado en el espejo de la puerta se enciende. En ese momento, si la luz de giro lateral parpadea, el indicador LED también parpadea para alertar al conductor.', 'https://exiauto.com/static/img/files/2019_10_02_11_24_39pradoprado.jpg', NULL, NULL, NULL, NULL, 'https://exiauto.com/static/img/files/2019_10_02_11_26_20Spec Sheet PRADO.pdf'),
(15, 6, 'Fortuner TRD Sportivo', 2020, 0, 'Automático', 1, '0', 0, 'La nueva Fortuner en sus dos versiones, 4x4 y 4x2, presenta la combinación perfecta entre elegancia, confort y un alto rendimiento capaz de operar con tranquilidad en condiciones extremas. La armonía del nuevo diseño presenta líneas redondeadas que proporcionan una sensación de fuerza y robustez con detalles deportivos, sin perder la elegancia y sofisticación de un vehículo de categoría superior.', 'Aire acondicionado automático.\r\nSistema de climatización automática digital.\r\n\r\n\r\nAire acondicionado trasero.\r\nSistema de enfriamiento trasero independiente para mayor confort en las dos filas de asientos posteriores.\r\n\r\n\r\nNumerosos espacios de almacenamiento.\r\nFortuner tiene diversos espacios de almacenamiento para colocar artículos que se deseen tener a la mano.\r\n\r\n\r\nConsola de techo con luces de lectura.\r\nLa consola superior entre los dos tapasoles está perfectamente adecuada para guardar los lentes de sol u otros artículos de tamaño similar. Igualmente posee doble lámparas de lectura para mayor confort del cliente.', 'Tablero central y paneles.\r\nDetalles internos cromados y apliques tipo madera oscura.\r\n\r\n\r\nInnovador panel de instrumento optitrón.\r\nEl panel de instrumentos tipo optitron permite al conductor tener una fácil lectura de cada uno de los componentes del tablero, permitiendo una conducción más segura.\r\n\r\n\r\nVolantes con controles de audio.\r\nLos controles de audio ubicados en el volante le permiten ajustar las diferentes funciones del sistema de audio (volúmen, estaciones, canciones y modo de reproducción) sin perder la visión en el camino, permitiendo su manejo seguro.\r\n\r\n\r\nTapicería de cuero.\r\n', 'Sistema inmobilizer\r\nPara mayor seguridad, la Fortuner está equipada con un sistema que bloquea el encendido.\r\n\r\n\r\nABS\r\nSistema de frenos antibloqueo-ABS\r\n\r\n\r\nDoble bolsa de aire\r\nDoble bolsa de aire para conductor y pasajero.\r\n\r\n\r\nCarrocería de seguridad\r\nLa carrocería de seguridad contra choques abarca un habitáculo de alta integridad, con zonas de absorción de impacto delantera y traseras, que minimizan la transmisión del impacto hacia los pasajeros.\r\n\r\n\r\nAlarma antirrobo.\r\nLa Fortuner tiene un sistema de seguridad que avisa en caso de un robo o el intento del mismo.', 'https://exiauto.com/static/img/files/2019_10_17_14_10_18fortuner trd sportivo.jpg', NULL, NULL, NULL, NULL, NULL),
(16, 8, 'Hilux Gr Sport', 2020, 0, 'Automático', 1, '1', 0, 'Todo lo que aprendimos de las competencias más exigentes llevado a una Pick-Up', 'Robusta y deportiva, con una parrilla imponente que destaca el emblema Toyota, barra y estribos de diseño exclusivo y llantas de aleación específicas.', 'Destacan los colores negro y rojo. Las butacas son de cuero natural ecológico, y todas vienen con un número único que las identifica. La nueva GR-Sport respira las carreras incluso desde el botón de arranque. ', 'Todas las versiones vienen equipadas en la parte delantera con una cámara frontal Dashcam que registra tus momentos en Full HD, y viene con GPS y WiFi incorporado.', 'https://exiauto.com/static/img/files/2020_02_05_09_14_11gr sport-3.jpg', 'https://exiauto.com/static/img/files/2020_02_05_09_14_11gr sport-4.jpg', 'https://exiauto.com/static/img/files/2020_02_05_09_14_11gr sport-5.jpg', 'https://exiauto.com/static/img/files/2020_02_05_09_14_11gr sport-6.jpg', 'https://exiauto.com/static/img/files/2020_02_05_09_14_12gr sport-55.jpg', 'https://exiauto.com/static/img/files/2020_02_05_09_14_12Hilux GR Sport - Specs.pdf');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `usuario` (`usuario`),
  ADD UNIQUE KEY `correo` (`correo`);

--
-- Indices de la tabla `agentes`
--
ALTER TABLE `agentes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `cod_age` (`cod_age`);

--
-- Indices de la tabla `asignar_modelos_repuestos`
--
ALTER TABLE `asignar_modelos_repuestos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_repuesto` (`id_repuesto`),
  ADD KEY `id_modelo` (`id_modelo`);

--
-- Indices de la tabla `categorias_repuestos`
--
ALTER TABLE `categorias_repuestos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `citas`
--
ALTER TABLE `citas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_usuario` (`id_usuario`),
  ADD KEY `id_vehiculo` (`id_vehiculo`),
  ADD KEY `id_agente` (`id_agente`),
  ADD KEY `id_falla` (`id_falla`),
  ADD KEY `id_kilometros` (`id_kilometros`);

--
-- Indices de la tabla `comentarios_reclamos`
--
ALTER TABLE `comentarios_reclamos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_usuario` (`id_usuario`),
  ADD KEY `id_admin` (`id_admin`),
  ADD KEY `id_reclamo` (`id_reclamo`);

--
-- Indices de la tabla `departamentos`
--
ALTER TABLE `departamentos`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nom_dep` (`nom_dep`);

--
-- Indices de la tabla `fallas`
--
ALTER TABLE `fallas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `kilometrajes`
--
ALTER TABLE `kilometrajes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `misvehiculos`
--
ALTER TABLE `misvehiculos`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `placa` (`placa`),
  ADD KEY `id_usuario` (`id_usuario`),
  ADD KEY `misvehiculos_ibfk_2` (`id_modelo`);

--
-- Indices de la tabla `modelos`
--
ALTER TABLE `modelos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `modelos_citas`
--
ALTER TABLE `modelos_citas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `modelos_repuestos`
--
ALTER TABLE `modelos_repuestos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `noticias`
--
ALTER TABLE `noticias`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `reclamos`
--
ALTER TABLE `reclamos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_usuario` (`id_usuario`),
  ADD KEY `id_departamento` (`id_departamento`);

--
-- Indices de la tabla `repuestos`
--
ALTER TABLE `repuestos`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `cod_rep` (`cod_rep`);

--
-- Indices de la tabla `solicitud_repuestos`
--
ALTER TABLE `solicitud_repuestos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ced_usu` (`ced_usu`),
  ADD UNIQUE KEY `cor_usu` (`cor_usu`);

--
-- Indices de la tabla `vehiculos`
--
ALTER TABLE `vehiculos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_modelo` (`id_modelo`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `agentes`
--
ALTER TABLE `agentes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `asignar_modelos_repuestos`
--
ALTER TABLE `asignar_modelos_repuestos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT de la tabla `categorias_repuestos`
--
ALTER TABLE `categorias_repuestos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `citas`
--
ALTER TABLE `citas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `comentarios_reclamos`
--
ALTER TABLE `comentarios_reclamos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT de la tabla `departamentos`
--
ALTER TABLE `departamentos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `fallas`
--
ALTER TABLE `fallas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=202;

--
-- AUTO_INCREMENT de la tabla `kilometrajes`
--
ALTER TABLE `kilometrajes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=231;

--
-- AUTO_INCREMENT de la tabla `misvehiculos`
--
ALTER TABLE `misvehiculos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT de la tabla `modelos`
--
ALTER TABLE `modelos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `modelos_citas`
--
ALTER TABLE `modelos_citas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `modelos_repuestos`
--
ALTER TABLE `modelos_repuestos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `noticias`
--
ALTER TABLE `noticias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `reclamos`
--
ALTER TABLE `reclamos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `repuestos`
--
ALTER TABLE `repuestos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `solicitud_repuestos`
--
ALTER TABLE `solicitud_repuestos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT de la tabla `vehiculos`
--
ALTER TABLE `vehiculos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `asignar_modelos_repuestos`
--
ALTER TABLE `asignar_modelos_repuestos`
  ADD CONSTRAINT `asignar_modelos_repuestos_ibfk_1` FOREIGN KEY (`id_repuesto`) REFERENCES `repuestos` (`id`),
  ADD CONSTRAINT `asignar_modelos_repuestos_ibfk_2` FOREIGN KEY (`id_modelo`) REFERENCES `modelos_repuestos` (`id`);

--
-- Filtros para la tabla `citas`
--
ALTER TABLE `citas`
  ADD CONSTRAINT `citas_ibfk_1` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`id`),
  ADD CONSTRAINT `citas_ibfk_2` FOREIGN KEY (`id_vehiculo`) REFERENCES `misvehiculos` (`id`),
  ADD CONSTRAINT `citas_ibfk_3` FOREIGN KEY (`id_agente`) REFERENCES `agentes` (`id`),
  ADD CONSTRAINT `citas_ibfk_4` FOREIGN KEY (`id_falla`) REFERENCES `fallas` (`id`),
  ADD CONSTRAINT `citas_ibfk_5` FOREIGN KEY (`id_kilometros`) REFERENCES `kilometrajes` (`id`);

--
-- Filtros para la tabla `comentarios_reclamos`
--
ALTER TABLE `comentarios_reclamos`
  ADD CONSTRAINT `comentarios_reclamos_ibfk_1` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`id`),
  ADD CONSTRAINT `comentarios_reclamos_ibfk_2` FOREIGN KEY (`id_admin`) REFERENCES `admins` (`id`),
  ADD CONSTRAINT `comentarios_reclamos_ibfk_3` FOREIGN KEY (`id_reclamo`) REFERENCES `reclamos` (`id`);

--
-- Filtros para la tabla `misvehiculos`
--
ALTER TABLE `misvehiculos`
  ADD CONSTRAINT `misvehiculos_ibfk_1` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `misvehiculos_ibfk_2` FOREIGN KEY (`id_modelo`) REFERENCES `modelos_citas` (`id`);

--
-- Filtros para la tabla `reclamos`
--
ALTER TABLE `reclamos`
  ADD CONSTRAINT `reclamos_ibfk_1` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`id`),
  ADD CONSTRAINT `reclamos_ibfk_2` FOREIGN KEY (`id_departamento`) REFERENCES `departamentos` (`id`);

--
-- Filtros para la tabla `vehiculos`
--
ALTER TABLE `vehiculos`
  ADD CONSTRAINT `vehiculos_ibfk_1` FOREIGN KEY (`id_modelo`) REFERENCES `modelos` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
