<?php
	error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING ^ E_DEPRECATED);
	session_start();
	include_once('ruta.php');
	include_once("configuraciones.php");
    include_once("panel/modelo/Vehiculo.php");
	if($_SERVER['SERVER_NAME']!="localhost"){	
	  if(empty($_SERVER['HTTPS']) || $_SERVER['HTTPS'] === "off") {
		  $location = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
		  header('HTTP/1.1 301 Moved Permanently');
		  header('Location: ' . $location);
		  exit;
	  }
	}
?>

<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="Toyocenter">
	<meta name="author" content="Plade Company C.A">
	<meta name="keywords" content="Vehículo, carros, comprar, ventas, Toyota, concesionario, Maracay, repuestos">
	<meta name="description" content="Toyocenter">

	<title>TOYOCENTER</title>

	<link href="static/img/favicon.ico" rel="icon">
	<link href="static/img/apple-touch-icon.png" rel="apple-touch-icon">

	<link rel="stylesheet" href="static/css/animate.css">
	
	<link rel="stylesheet" href="static/css/owl.carousel.min.css">
	<link rel="stylesheet" href="static/css/owl.theme.default.min.css">
	<link rel="stylesheet" href="static/css/flaticon.css">
	<link rel="stylesheet" href="static/css/icomoon.css">
	<link rel="stylesheet" href="static/css/style.css">
	<link rel="stylesheet" href="static/css/smoothproducts.css">

	<link type="text/css" rel="stylesheet" media="screen,projection" href="static/lib/JTables/css/jquery.dataTables.min.css">
	<link type="text/css" rel="stylesheet" media="screen,projection" href="static/lib/JTables/css/dataTables.bootstrap.css">
	<link type="text/css" rel="stylesheet" media="screen,projection" href="static/lib/JTables/css/responsive.dataTables.min.css">

	<script src='fullcalendar/packages/moment.js'></script>
</head>

<body>
	<div class="bg-top navbar-light d-flex flex-column-reverse">
		<div class="container py-3">
			<div class="row no-gutters d-flex align-items-center align-items-stretch">
				<div class="col-md-4 d-flex align-items-center py-4">
					<a class="navbar-brand" href="index.php"><img src="static/img/logo.jpg" width="15%"> TOYOCENTER <span>RIF J-30861260-0</span></a>
				</div>
				<div class="col-lg-8 d-block">
					<div class="row d-flex">
						<div class="col-md d-flex topper align-items-center align-items-stretch py-md-4">
							<div class="icon d-flex justify-content-center align-items-center"><span class="icon icon-envelope"></span></div>
							<div class="text">
								<span>Correo</span>
								<span>contacto@toyocenter.com</span>
							</div>
						</div>
						<div class="col-md d-flex topper align-items-center align-items-stretch py-md-4">
							<div class="icon d-flex justify-content-center align-items-center"><span class="icon icon-phone"></span></div>
							<div class="text">
								<span>Teléfonos</span>
								<span>0243-2334937</span>
							</div>
						</div>
						<div class="col-md d-flex topper align-items-center align-items-stretch py-md-4">
							<div class="icon d-flex justify-content-center align-items-center"><span class="icon icon-clock-o"></span></div>
							<div class="text">
								<span>Horario</span>
								<span>Lun - Vie 8am - 12pm / 1:30pm - 5:30pm</span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<nav class="navbar navbar-expand-lg navbar-dark bg-dark ftco-navbar-light" id="ftco-navbar">
		<div class="container d-flex align-items-center">
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
				<span class="oi oi-menu"></span> Menu
			</button>

			<div class="collapse navbar-collapse" id="ftco-nav">
				<ul class="navbar-nav mr-auto">
					<li class="nav-item active"><a href="index.php" class="nav-link">Inicio</a></li>
					<li class="nav-item"><a href="index.php#nosotros" class="nav-link">Nosotros</a></li>
					<li class="nav-item"><a href="index.php#servicio" class="nav-link">Servicio</a></li>
					<li class="nav-item"><a href="?op=vehiculos" class="nav-link">Vehículos</a></li>
					<li class="nav-item"><a href="?op=repuestos" class="nav-link">Repuestos</a></li>
					<li class="nav-item"><a href="index.php#noticias" class="nav-link">Noticias</a></li>
					<li class="nav-item"><a href="?op=contacto" class="nav-link">Contacto</a></li>
					<?php
						if(isset($_SESSION['log']) && $_SESSION['log'] == true){
					}else{
					?>
						<li class="nav-item"><a href="#" class="nav-link" data-toggle="modal" data-target="#md-ingresar"><span>Ingresar</span></a></li>
					<?php }?>

					<?php
						if($promo['est'] == "1"){
					?>
						<li class="nav-item"><a href="#" id="bt_promo" class="btn btn-primary">Promo</a></li>
					<?php } ?>

					<?php
					if(isset($_SESSION['log'])&&$_SESSION['log'] == true){
					?>
						<button onclick="window.location='?op=inicio_log';" type="button" class="btn nav-item" style="color:#fff;"> <b><?php echo "Hola ". $_SESSION['nom'];?></b> </button>
					<?php
					}else{
					?>
						<li class="nav-item">
							<a href="#" id="bt_registro_modal" class="nav-link" data-toggle="modal" data-target="#md-registro">Regístrate</a>
						</li>
					<?php }?>
				</ul>
			</div>
		</div>
	</nav>
	<!-- END nav -->

	<div>
		<?php include_once($ruta); ?>
	</div>

	<?php include_once("modales.php");?>

	<footer class="ftco-footer ftco-bg-dark ftco-section">
		<div class="container">
			<div class="row mb-5">
				<div class="col-md-6 col-lg-3">
					<div class="ftco-footer-widget mb-5">
						<h2 class="ftco-heading-2">Encuéntranos</h2>
						<div class="block-23 mb-3">
							<ul>
								<li><span class="icon icon-map-marker"></span><span class="text">Av. Bolivar Este #82 sector centro frente al liceo Militar; diagonal al C.C Parque Aragua; Entrada por la Av. Miranda, a 50 metros de Av. Fuerza Aérea.</span></li>
							</ul>
						</div>
						<ul class="ftco-footer-social list-unstyled float-md-left float-lft mt-3">
							<!--<li class="ftco-animate"><a href="#"><span class="icon-twitter"></span></a></li>
							<li class="ftco-animate"><a href="#"><span class="icon-facebook"></span></a></li>-->
							<li class="ftco-animate"><a href="https://www.instagram.com/toyocenter_maracay/" target="_blank"><span class="icon-instagram"></span></a></li>
						</ul>
					</div>
				</div>

				<div class="col-md-6 col-lg-6">
					<div class="ftco-footer-widget mb-5">
						<h2 class="ftco-heading-2">Teléfonos</h2>
						<div class="row">
							<div class="col-md-4">
								<div class="block-23 mb-3">
								<ul>
									<li><a href="#"><span class="icon icon-phone"></span><span class="text"><b>CENTRAL:</b><br> 0243-2331465 <br> 0243-2331467 <br> 0243-2331468</span></a></li>
								</ul>
								</div>
							</div>
							<div class="col-md-4">
								<div class="block-23 mb-3">
								<ul>
									<li><a href="#"><span class="icon icon-phone"></span><span class="text"><b>REPUESTOS</b><br> 0243-2331397 <br> 0243-2320718 <br>0243-2332321</span></a></li>
								</ul>
								</div>
							</div>
							<div class="col-md-4">
								<div class="block-23 mb-3">
								<ul>
									<li><a href="#"><span class="icon icon-phone"></span><span class="text"><b>SERVICIO:</b><br> 0243-2334937</span></a></li>
								</ul>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="col-md-6 col-lg-3">
					<div class="ftco-footer-widget mb-5 ml-md-4">
					<h2 class="ftco-heading-2">Enlaces</h2>
					<ul class="list-unstyled">
						<li><a href="#servicio"><span class="ion-ios-arrow-round-forward mr-2"></span>Servicio</a></li>
						<li><a href="#repuestos"><span class="ion-ios-arrow-round-forward mr-2"></span>Repuestos</a></li>
						<li><a href="#vehiculos"><span class="ion-ios-arrow-round-forward mr-2"></span>Vehículos</a></li>
					</ul>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-12 text-center">
					<p>Copyright &copy;<script>document.write(new Date().getFullYear());</script> Todos los derechos reservados y desarrollado por <a href="https://pladecompany.com" target="_blank"><b>Plade Company C.A</b></a>
					</p>
				</div>
			</div>
		</div>
	</footer>
	
	<!-- loader
	<div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div> -->



	<script src="static/js/jquery.min.js"></script>
	<script src="static/js/jquery-migrate-3.0.1.min.js"></script>
	<script src="static/js/bootstrap.min.js"></script>
	<script src="static/js/jquery.easing.1.3.js"></script>
	<script src="static/js/jquery.waypoints.min.js"></script>
	<script src="static/js/jquery.stellar.min.js"></script>
	<script src="static/js/owl.carousel.min.js"></script>
	<script src="static/js/aos.js"></script>
	<script src="static/js/jquery.animateNumber.min.js"></script>
	<script src="static/js/scrollax.min.js"></script>
	<script src="static/js/main.js"></script>
	<script src="static/js/registro.js"></script>

	<script type="text/javascript" src="static/lib/JTables/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="static/lib/JTables/js/dataTables.bootstrap.js"></script>
	<script type="text/javascript" src="static/lib/JTables/js/dataTables.responsive.min.js"></script>

	<script type="text/javascript" src="static/js/smoothproducts.min.js"></script>
	<script type="text/javascript">

	$(window).load(function() {
		$('.sp-wrap').smoothproducts();
	});
	</script>
	
	<script>
		$(document).ready(function(){
			$('.table').DataTable( {
				"language": {
					"url": "static/lib/JTables/Spanish.json"
				}
			} );
			$('.table').DataTable( {
				responsive: true
			} );
		} );
	</script>

	<script type="text/javascript">

		$('#myModal').on('shown.bs.modal', function () {
			$('#myInput').trigger('focus')
		})

		<?php
		  if(isset($_GET['reg'])){
		?>
			$("#bt_registro_modal").trigger('click');
		<?php
			if(isset($_GET['msj'])){
		?>
			$("#cont_msj").show();
			$("#txt_msj").text("** <?php echo $_GET['msj'];?> **");
		<?php
			}
		  }

		  if($promo['est'] == "1"){ // Si la promocion es visible desde el panel admin entra acá
			// Validamos la session, si existe la sesion no mostramos modal, cuando es primera vez si lo mostramos.
			if( isset($_SESSION['ok']) ){
			}else{
			  $_SESSION['ok'] = true;
		?>
			$('#md-video').modal('show');
			//$('#video_modal').trigger('play');
		<?php
			}
		  }
		?>


	  $(document).on('ready', function(){
		$("#bt_promo").on('click', function(){
			$('#md-video').modal('show');
		});

	  <?php
		if(isset($_GET['clave'])){
	  ?>
		  $("#md-ingresar").modal("show");
		  $('#msj_login').text('Su contraseña de ingreso se envio a su correo, verifiquela y vuelva ingresar');
	  <?php
		}
	  ?>
	  });

	  $(document).on('click', '#bt_reg_log', function(){
		$("#md-ingresar").modal("hide");
		$("#bt_registro_modal").trigger('click');
	  });

	  <?php
		if(isset($_GET['err']) && !isset($_GET['reg'])){
		  ?>
		  $("#bt_ingresar").trigger('click');
		  var html = "Datos invalidos, desea registrarse ? <input type='button' class='btn btn-success' value='Registrarse' id='bt_reg_log'>";
		  $("#msj_login").html(html);
		  <?php
		}
	  ?>
	  <?php
		if(isset($_GET['olvido'])){
		  ?>
		  $("#md-olvido").modal("show");
		  <?php
		}
	  ?>


	</script>
</body>
</html>
