$(document).ready(function(){
  $("#formulario_registro_cliente").on('submit', function(){
    var pas = $("input[name='pas']").val().trim();
    var cpa = $("input[name='cpa']").val().trim();
    if(pas != cpa){
      $("#cont_msj").show();
      $("#txt_msj").text("** Las contraseñas no coinciden **");
      $("input[name='pas']").focus();
      return false;
    }else{
      $("#cont_msj").hide();
      return true;
    }
  });
});
