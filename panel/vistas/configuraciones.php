<?php
  $file = "config.json";
  $json = json_decode(file_get_contents($file), true);
  $promo = $json["promo"];
  $video = $json["video"];
  $whatsapp = $json["whatsapp"];

  include_once("controlador/configuraciones.php");
?>
<div class="card shadow mb-4">
	<div class="card-header py-3">
		<h4 class="m-0 font-weight-bold color-b">Configurar número de teléfono de whatsapp</h4>
		
		<div class="text-right">

		</div>
	</div>

	<div class="card-body">
          <form class="form-a" method="POST" action="" enctype="multipart/form-data" id="">
            <div class="row">
              <div class="col-md-4">
                <label>Número de teléfono</label>
                <input type="text" name="tlf" class="form-control" placeholder="" required value="<?php echo $whatsapp["tlf"]; ?>">
              </div>
            </div>
            <div class="row" style="margin-top:2em;">
              <div class="col-md-12 text-center">
                <input type="submit" name="bt_whatsapp" class="btn btn-success pull-right" value="Guardar información">
              </div>
            </div>
          </form>
    </div>
</div>

<div class="card shadow mb-4">
	<div class="card-header py-3">
		<h4 class="m-0 font-weight-bold color-b">Configurar video de la web</h4>
		
		<div class="text-right">

		</div>
	</div>

	<div class="card-body">
          <form class="form-a" method="POST" action="" enctype="multipart/form-data" id="">
            <div class="row">
              <div class="col-md-4">
                <label>Título del video</label>
                <input type="text" name="tit" class="form-control" placeholder="Ejemplo: Ruta Toyota" required value="<?php echo $video["titulo"]; ?>">
              </div>
              <div class="col-md-4">
                <label>Url del video youtube</label>
                <input type="text" name="cod" class="form-control" id="cod_video" placeholder="Ejemplo: Ruta Toyota" required value="<?php echo $video["video"]; ?>">
              </div>
            </div>
            <div class="row" style="margin-top:2em;">
              <div class="col-md-12 text-center">
                <input type="submit" name="bt_video" class="btn btn-success pull-right" value="Guardar información">
              </div>
            </div>
          </form>
    </div>
</div>

<div class="card shadow mb-4">
	<div class="card-header py-3">
		<h4 class="m-0 font-weight-bold color-b">Configurar promoción</h4>
		
		<div class="text-right">
		</div>
	</div>

	<div class="card-body">
        <form class="form-a" method="POST" action="" enctype="multipart/form-data" id="">
          <div class="row">
            <div class="col-md-4">
              <label>Titulo de la promoción</label>
              <input type="text" name="tit" class="form-control" placeholder="Ejemplo: Ruta Toyota" required value="<?php echo $promo["titulo"]; ?>">
            </div>
            <div class="col-md-4">
              <label>Tipo de la promoción</label>
              <select name="tip" class="form-control" required>
                <option value="">Seleccionar</option>
                <option <?php echo (($promo['tipo'] == 'video')?'selected':''); ?> value="video">Video</option>
                <option <?php echo (($promo['tipo'] == 'imagen')?'selected':'');?> value="imagen">Imagén</option>
              </select>
            </div>
            <div class="col-md-4">
              <label>Estatus de la promoción</label>
              <select name="est" class="form-control" required>
                <option value="">Seleccionar</option>
                <option <?php echo (($promo['est'] == 1)?'selected':''); ?> value="1">Visible</option>
                <option <?php echo (($promo['est'] == 0)?'selected':'');?> value="0">No visible</option>
              </select>
            </div>
          </div>

          <div class="row" style="margin-top:2em;">
            <div class="col-md-4">
              <label for="img">Archivo/Imagén</label>
              <input type="file" id="img" class="form-control" name="img" >
            </div>
          </div>

          <div class="row" style="margin-top:2em;">
            <div class="col-md-12 text-center">
              <input type="submit" name="bt_promo" class="btn btn-success pull-right" value="Guardar información">
            </div>
          </div>
          <hr>
          <div class="row" style="margin-top:2em;">
            <div class="col-md-5">
              <?php if($promo['tipo'] == 'video'){ ?>
                <label for="img">Video establecido para la promoción</label>
                <video src="<?php echo $promo['url'];?>" style="width:100%;" controls>
              <?php }else if($promo['tipo'] == 'imagen'){ ?>
                <label for="img">Imagén establecido para la promoción</label>
                <img src="<?php echo $promo['url'];?>" style="width:100%;">
              <?php }?>
            </div>
          </div>
        </form>
    </div>
</div>
<script>
  $(document).ready(function(){
    $("#cod_video").on('change', function(){
      var url = $("#cod_video").val();
      console.log(url);
      var cod = url.substring(url.indexOf("=")+1, url.length);
      if(cod.length != 0){
        $("#cod_video").val(cod);
      }
    });
  });
</script>
